<?php

namespace Project\Collection;

use ReflectionClass;

class AbstractCollection
{
    private $class;
    private $collection = [];

    public function __construct($class)
    {
        $this->setClass($class);
    }

    public function getItem($key)
    {
        if(!isset($this->collection[$key])) { return false; }
        return $this->collection[$key];
    }

    public function forEach($function): AbstractCollection
    {
        foreach ($this->getCollection() as $key => $value) {
            $function($value, $key);
        }
        return $this;
    }

    public function updateItem($key, $data): bool
    {
        if(!isset($this->collection[$key])) { return false; }
        $this->addToCollection($data, $key);
        return true;
    }

    public function getFirst()
    {
        return $this->getItem(array_key_first($this->collection));
    }

    public function getLast()
    {
        return $this->getItem(array_key_last($this->collection));
    }

    public function getMap(): array
    {
        return array_keys($this->collection);
    }

    public function addToCollection($object, $key = false, $multiple = false): AbstractCollection
    {
        if($multiple) {
            if(!is_array($object)) { return $this; }
            foreach ($object as $key => $value) {
                $this->addToCollection($value, false);
            }
            return $this;
        }

        if(is_array($object)) {
            $class = $this->getClass()->newInstance();
            $class->bindParametersToObject($object);
            $object = $class;
        }

        if(!$this->isObjectValid($object)) { return $this; }

        if($key === false) { $this->collection[] = $object; }
        else { $this->collection[$key] = $object; }

        return $this;
    }

    public function isObjectValid($object): bool
    {
        $className = $this->getClass()->getName();
        return $object instanceof $className;
    }

    /**
     * @return ReflectionClass
     */
    public function getClass(): \ReflectionClass
    {
        return $this->class;
    }

    /**
     * @param object[string $class
     * @return AbstractCollection
     */
    public function setClass($Reflection)
    {
        try {
            $Reflection = new \ReflectionClass($Reflection);
            $this->class = $Reflection;
        } catch (\ReflectionException $e) {

        }

        return $this;
    }

    /**
     * @return array
     */
    public function getCollection(): array
    {
        return $this->collection;
    }

    /**
     * @param array $collection
     * @return AbstractCollection
     */
    public function setCollection(array $collection): AbstractCollection
    {
        foreach ($collection as $key => $object) {
            $this->addToCollection($object, $key);
        }
        return $this;
    }
}