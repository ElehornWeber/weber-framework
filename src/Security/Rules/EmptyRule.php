<?php
namespace Project\Security\Rules;

class EmptyRule extends AbstractRule
{
    public function __construct()
    {
       $this->setId('EMPTY_RULE');
       $this->setName("Règle de sécurité vide");
    }
    public function action()
    {
        // TODO: Implement action() method.
    }

    public function process(): bool
    {
        return true;
    }

}