<?php
namespace Project\Security;

use Core\CoreConnexionManager;

class ConnexionManager extends CoreConnexionManager
{
    static function initializeConnexion()
    {
        parent::initializeConnexion();
    }
}