<?php

namespace Project\Security;

class HttpResponse extends Response
{
    static function createHttpResponse($code, $content, $contentType = 'application/json')
    {
        if(!static::isCli()) { http_response_code($code); }
        if($contentType === 'application/json' || $contentType === 'json') {
            if(!static::isCli()) { header(sprintf('Content-type: %s', static::JSON)); }
            echo json_encode($content);
        } else if($contentType === 'text/html' || $contentType === 'text') {
            if(!static::isCli()) { header(sprintf('Content-type: %s', static::TEXT)); }
            echo $content;
        }

        return $code;
    }


}