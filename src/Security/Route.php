<?php
namespace Project\Security;

use Project\PageManager;

class Route {
    private $file = '';
    private $slug = '';
    private $method = '';
    private $security_level = [];
    private $arguments = [];

    public function isComplete(): bool
    {
        if($this->getFile()) { return true; }

        if($this->getSlug()) { return true; }
        return false;
    }

    public function getKey(): string
    {
        if(!$this->getMethod()) { return $this->getFile(); }
        return sprintf('%s:%s', $this->getFile(), $this->getMethod());
    }

    public function getNiceTitle()
    {
        $title = str_replace('-',' ', $this->getSlug());
        $title = str_replace('/',': ', $title);
        $title = ucwords($title);
        return $title;
    }

    /**
     * @return mixed
     */
    public function getFile()
    {
        return $this->file;
    }

    /**
     * @param mixed $file
     * @return Route
     */
    public function setFile($file)
    {
        $this->file = $file;
        return $this;
    }

    /**
     *
     * Get the slug of the current Route.
     * If $this has Arguments, bind them to the slug
     *
     * @return mixed
     */
    public function getSlug($args = [], $simple = false)
    {
        if($simple) { return $this->slug; }
        if(!$this->getArguments()) { return $this->slug; }

        $complexeSlug = preg_replace(['/(\&[0-9])/'], $args, $this->slug );
        $complexeSlug = str_replace('//', '/', $complexeSlug);
        return $complexeSlug;
    }

    public function getRegexSlug(): string
    {
        if(!$this->getArguments()) { return $this->getSlug(null, true); }

        $result = $this->slug;
        $argumentsOrder = array_flip(array_keys($this->getArguments()));
        foreach($this->getArguments() as $key => $argument) {
            $i = $argumentsOrder[$key] + 1;
            $result = preg_replace('/(\&[' . $i . '])/', $argument, $result);
        }
        if(!$result) { return $this->getSlug(); }
        return $result;
    }

    public function getKeyedSlug()
    {
        $arguments = [];
        $i = 1;
        foreach ($this->getArguments() as $key => $regex) { $arguments['&'.$i] = sprintf('$%s', $key); $i++; }
        if(!$this->getArguments()) { return $this->getSlug(null, true); }

        $result = $this->slug;
        foreach($arguments as $index => $argument) {
            $result = str_replace($index, $argument, $result);
        }

        if(!$result) { return $this->getSlug(); }

        return $result;
    }

    public function extractParameters(): array
    {
        $page = PageManager::getPage();
        preg_match_all(sprintf('@^%s$@', $this->getRegexSlug()), $page, $matches);

        $results = [];
        $i = 1;
        foreach ($this->getArguments() as $key => $pattern) {
            if(!isset($matches[$i])) {
                $results[$key] = $pattern;
                $i++;
                continue;
            }

            if(!isset($matches[$i][0])) { continue; }
            $results[$key] = $matches[$i][0];
            $i++;
        }

        return $results;
    }

    /**
     * @param mixed $slug
     * @return Route
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;
        return $this;
    }

    /**
     * @return array
     */
    public function getSecurityLevel(): array
    {
        return $this->security_level;
    }

    /**
     * @param mixed $security_level
     * @return Route
     */
    public function setSecurityLevel($security_level): Route
    {
        if(!is_array($security_level)) { $this->security_level = [$security_level]; }
        else { $this->security_level = $security_level; }

        return $this;
    }

    public function getArgument($key)
    {
        if(!isset($this->arguments[$key])) { return null; }
        return $this->arguments[$key];
    }

    /**
     * @return array
     */
    public function getArguments(): array
    {
        return $this->arguments;
    }

    /**
     * @param array $arguments
     * @return Route
     */
    public function setArguments(array $arguments): Route
    {
        $this->arguments = $arguments;
        return $this;
    }

    /**
     * @return string
     */
    public function getMethod(): string
    {
        return $this->method;
    }

    /**
     * @param string $method
     * @return Route
     */
    public function setMethod(string $method): Route
    {
        $this->method = $method;
        return $this;
    }
}