<?php
namespace Project\Security;

use Core\CorePageManager;
use Project\PageManager;
use Project\Security\Rules\AbstractRule;
use Project\Security\Rules\EmptyRule;
use Project\Utilities\SimpleDataPerfTest;

abstract class AbstractSecurity
{
    protected $rules = [];
    protected $resultCache = [];
    protected $cacheUse = 0;

    /*
 * Get a rule defined in the Security Process
 * + Rules finded automaticaly are now stored: We gain some ultra minor perf, but better
 */
    public function getRule($rule): AbstractRule
    {
        if($rule instanceof AbstractRule) {
            return $rule;
        }

        //First step: We try to get a manually defined rule
        if(isset($this->rules[$rule])) {
            SimpleDataPerfTest::incrementData('rule_cache', 1);
            return new $this->rules[$rule]();
        }

        if(class_exists($rule) && is_subclass_of($rule, AbstractRule::class)) {
            $class = new $rule();
            $this->rules[$rule] = $rule;
            return $class;
        }

        //If this rule doesn't exist, we'll try to find if the rule exists somewhere
        $ruleNameParsed = str_replace('_',' ', $rule);
        $ruleNameParsed = strtolower($ruleNameParsed);
        $ruleNameParsed = ucwords($ruleNameParsed);
        $ruleNameParsed = str_replace(' ', '', $ruleNameParsed);

        $completeNameSpace = sprintf('Project\\Security\\Rules\\%s', $ruleNameParsed);

        if(class_exists($completeNameSpace)) {
            $class = new $completeNameSpace();
            if($class instanceof AbstractRule) {
                $this->rules[$rule] = $class;
                return $class;
            }
        }

        //Finally, if this rule doesn't exist after all, return the Empty Rule to prevent error
        return new EmptyRule();
    }

    public function checkSecurity($securityRules): bool
    {
        if(!is_array($securityRules)) { $securityRules = [$securityRules]; }

        foreach ($securityRules as $security) {
            $rule = $this->getRule($security);
            if(!$rule->startRule()) { return false; }
        }
        return true;
    }

    public function checkCurrentRouteSecurity()
    {
        $Route = PageManager::getRouter()->getCurrentRoute();

        foreach ($Route->getSecurityLevel() as $security) {
            $rule = $this->getRule($security);
            if ($rule->startRule()) { continue; }

            ConnexionManager::addFlashSessionMessage('warning', 'Accès refusé.', 1);

            return PageManager::goToLastPage();
        }
        return true;
    }

    public function checkRouteSecurity($Route): bool
    {
        if(!$Route instanceof Route) { $Route = PageManager::getRouter()->getRoute($Route); }

        foreach ($Route->getSecurityLevel() as $security) {
            $rule = $this->getRule($security);
            if(!$rule->startRule()) { return false; }
        }

        return true;
    }

    public function addRule($rule, $key = '') {
        if($rule instanceof AbstractRule) {
            $this->rules[get_class($rule)] = $rule;
            return;
        }

        if($key) {
            $this->rules[$key] = $rule;
            return;
        }
    }

    /**
     * @return array
     */
    public function getRules(): array
    {
        return $this->rules;
    }

    /**
     * @param array $rules
     * @return AbstractSecurity
     */
    public function setRules(array $rules): AbstractSecurity
    {
        $this->rules = $rules;
        return $this;
    }


    public function isResultCached($rule): bool
    {
        if($rule instanceof AbstractRule) {
            if(isset($this->resultCache[$rule->getId()])) { return true; }
            if(isset($this->resultCache[get_class($rule)])) { return true; }
            return false;
        }
        if(!isset($this->resultCache[$rule])) { return false; }
        return true;
    }

    public function getResultCache($rule): bool
    {
        $this->cacheUse++;
        if($rule instanceof AbstractRule) {
            if(isset($this->resultCache[$rule->getId()])) { return $this->resultCache[$rule->getId()]; }
            if(isset($this->resultCache[get_class($rule)])) { return $this->resultCache[get_class($rule)]; }
            return false;
        }
        if(!isset($this->resultCache[$rule])) { return false; }
        return $this->resultCache[$rule];
    }

    public function addResultCache($rule, $result) {
        if($rule instanceof AbstractRule) {
            $this->resultCache[$rule->getId()] = $result;
            return;
        }
        $this->resultCache[$rule] = $result;
        return;
    }

    /**
     * @return array
     */
    public function getResultsCache(): array
    {
        return $this->resultCache;
    }

    /**
     * @param array $resultCache
     * @return AbstractSecurity
     */
    public function setResultCache(array $resultCache): AbstractSecurity
    {
        $this->resultCache = $resultCache;
        return $this;
    }
}