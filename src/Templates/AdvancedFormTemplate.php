<?php
namespace Project\Templates;

use Project\Models\AbstractDbClass;
use Project\PageManager;
use Project\Security\ConnexionManager;

/**
 * $schemaElement =  [  name, required, type, class  ]
 */

class AdvancedFormTemplate extends AbstractTemplate {

    protected static function renderTextArea($schemaElement, $params = [])
    {
        $default = '';
        if(isset($schemaElement['params']['default'])) { $default = $schemaElement['params']['default']; }
        $schemaAttributes = [];
        if(isset($schemaElement['params']['attributes'])) { $schemaAttributes = $schemaElement['params']['attributes']; }

        $sectionClasses = "form-group mb-0";
        if(isset($params['sectionClasses'])) {
            $sectionClasses = $params['sectionClasses'];
            if(!$params['sectionClasses']) { $sectionClasses = ''; }
        }
        ?>
        <div class="<?= $sectionClasses; ?>">
            <?php if($schemaElement['params']['label'] && !isset($params['nolabel'])): ?>
                <label class="bmd-label-floating" for="<?=$schemaElement['completeName']; ?>"><?= $schemaElement['params']['label'];?></label>
            <?php endif; ?>
            <textarea
                    id="<?=$schemaElement['completeName']; ?>"
                    name="<?=$schemaElement['completeName']; ?>"
                    data-property="<?= $schemaElement['name']?>"
                    class="form-control form-text <?= $schemaElement['params']['classes']; ?>"
            <?php foreach ($schemaAttributes as $key => $attribute): ?>
                <?=$key;?>="<?= $attribute; ?>"
            <?php endforeach; ?>
            <?= static::renderCondition($schemaElement['params']['required'], 'required="required"'); ?>
            ><?= $schemaElement['value'] ?: $default; ?></textarea>
        </div>
        <?php
    }


    protected static function renderInput($schemaElement, $params = [])
    {
        $default = '';
        if(isset($schemaElement['params']['default'])) { $default = $schemaElement['params']['default']; }

        $schemaAttributes = [];
        if(isset($schemaElement['params']['attributes'])) { $schemaAttributes = $schemaElement['params']['attributes']; }

        $sectionClasses = "form-group mb-0";
        if(isset($params['sectionClasses'])) {
            $sectionClasses = $params['sectionClasses'];
            if(!$params['sectionClasses']) { $sectionClasses = ''; }
        }
        ?>
        <div class="<?= $sectionClasses; ?>">
            <?php if($schemaElement['params']['label'] && !isset($params['nolabel'])): ?>
                <label class="bmd-label-floating" for="<?=$schemaElement['completeName']; ?>"><?= $schemaElement['params']['label'];?></label>
            <?php endif; ?>

            <input
                    type="<?=$schemaElement['type']; ?>"
                    id="<?=$schemaElement['completeName']; ?>"
                    name="<?=$schemaElement['completeName']; ?>"
                    data-property="<?= $schemaElement['name']?>"
                    class="form-control <?= $schemaElement['params']['classes']; ?>"
                    value="<?= $schemaElement['value'] ?: $default; ?>"
            <?php foreach ($schemaAttributes as $key => $attribute): ?>
                <?=$key;?>="<?= $attribute; ?>"
            <?php endforeach; ?>
            <?= static::renderCondition($schemaElement['params']['required'], 'required="required"'); ?>
            >
        </div>
        <?php
    }

    protected static function renderSelectEnumOptions($schemaElement)
    {
        foreach ($schemaElement['params']['selectEnum'] as $key => $option): ?>
            <option value="<?= $key ?>" <?= static::renderCondition($schemaElement['value'] != '' && $key == $schemaElement['value'], 'selected="selected"')?>>
                <?= $option['name']; ?>
            </option>
        <?php endforeach;
    }


    protected static function renderSelectModelOptions($schemaElement)
    {
        $targetModel = $schemaElement['params']['Model'];
        if(!class_exists($targetModel)) { return false; }

        $targetModel = new $targetModel();
        if(!$targetModel instanceof AbstractDbClass) { return false; }

        $internalCondition = [];
        if(isset($schemaElement['params']['modelValues']) && $schemaElement['params']['modelValues']) {
            if(!is_array($schemaElement['params']['modelValues'])) {
                $schemaElement['params']['modelValues'] = [$schemaElement['params']['modelValues']];
            }
            $internalCondition[] = sprintf('id IN (%s)', implode(',', $schemaElement['params']['modelValues']));
        }

        $Models = $targetModel::getDao()->{'get'.$targetModel->getMultipleName()}($internalCondition);

        if(!isset($schemaElement['params']['modelTargetProperty'])) { $schemaElement['params']['modelTargetProperty'] = $targetModel->getUniqueIdProperty(); }

        /**@var AbstractDbClass $Model*/
        foreach ($Models as $Model): ?>
            <option value="<?= $Model->getId(); ?>" <?= static::renderCondition($schemaElement['value'] != '' && $Model->getUniquePropertyValue() == $schemaElement['value'], 'selected="selected"')?>>
                <?= $Model->dynamicGetterByProp($schemaElement['params']['modelTargetProperty']); ?>
            </option>
        <?php endforeach;
    }

    protected static function renderSelect($schemaElement, $params = [])
    {
        $sectionClasses = "form-group mb-0";
        if(isset($params['sectionClasses'])) {
            $sectionClasses = $params['sectionClasses'];
            if(!$params['sectionClasses']) { $sectionClasses = ''; }
        }
        ?>
        <div class="<?= $sectionClasses; ?>">
            <?php if($schemaElement['params']['label'] && !isset($params['nolabel'])): ?>
                <label class="bmd-label-floating" for="<?=$schemaElement['completeName']; ?>"><?= $schemaElement['params']['label'];?></label>
            <?php endif; ?>
            <select
                    id="<?=$schemaElement['completeName']; ?>"
                    name="<?=$schemaElement['completeName']; ?>"
                    class="form-control <?= $schemaElement['params']['classes']; ?>"
                    data-property="<?= $schemaElement['name']?>"
                <?= static::renderCondition($schemaElement['params']['required'], 'required="required"'); ?>
            >
                <option value="" selected><?php
                    if(!isset($params['nodefaultoption'])):
                        if(isset($schemaElement['params']['label'])): echo $schemaElement['params']['label'];
                        else : echo 'Sélectionnez';
                        endif;
                    endif;?>
                </option>
                <?php
                if(isset($schemaElement['params']['selectEnum'])):
                    static::renderSelectEnumOptions($schemaElement);
                elseif(isset($schemaElement['params']['Model']) && new $schemaElement['params']['Model']() instanceof AbstractDbClass):
                    static::renderSelectModelOptions($schemaElement);
                endif;
                ?>
            </select>
        </div>

        <?php
    }


    static public function renderElement($schemaElement, $params = [])
    {
        if($schemaElement['type'] == 'select') {
            static::renderSelect($schemaElement, $params);
            return;
        }
        if($schemaElement['type'] == 'textarea') {
            static::renderTextArea($schemaElement, $params);
            return;
        }

        static::renderInput($schemaElement, $params);
    }
}