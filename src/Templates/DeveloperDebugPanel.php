<?php
namespace Project\Templates;

use Project\PageManager;
use Project\Utilities\DataConverter;
use Project\Utilities\DateConverter;
use Project\Utilities\ErrorCatcher;
use Project\Utilities\SimpleDataPerfTest;
use Project\Utilities\SimpleTimePerfTest;
use Project\Utilities\SqlPerfTest;

class DeveloperDebugPanel extends AbstractTemplate
{
    /**
     *
     * @Todo : This is the Todo Main List for every process improvement !
     * @Todo: ASAP : getControllers on route instead of pagename pattern: So a route has only one controller -> improve security
     *
     * @param SimpleTimePerfTest $PerformanceMainPageProcess
     */
    static public function renderPanel(SimpleTimePerfTest $PerformanceMainPageProcess)
    {
        $route = PageManager::getRouter()->getRoute(PageManager::getFullPage());
        $controller = false;
        if($route) { $controller = $route->getFile().'Controller'; }

        $totalProcessingTime = $PerformanceMainPageProcess->getTestResult();
        $iterationAmount = 0; $perfColor = 'green';
        if($totalProcessingTime < 15 ) { $iterationAmount = 4; }
        else if($totalProcessingTime < 20 ) { $iterationAmount = 3; $perfColor = 'lightgreen'; }
        else if($totalProcessingTime < 30 ) { $iterationAmount = 2; $perfColor = 'orange';  }
        else if($totalProcessingTime < 40 ) { $iterationAmount = 1; $perfColor = 'red'; }
        ?>
        <section id="developerPanel">
            <div class="my-1 card card-body">
                <h5 class="m-0">Outil de développement</h5>
            </div>
            <div class="my-1">
                <div class="card card-body p-2">
                    <p class="m-0">
                        <strong>Temps total d'exécution de la page</strong> <?= $totalProcessingTime;?> ms
                        <?php for($i = 0; $i < $iterationAmount; $i++): ?>
                            <span class=" d-inline-block" style="margin-right: -2px;width: 3px; height: <?= 5*(1+$i); ?>px; background-color: <?= $perfColor;?>;"></span>
                        <?php endfor; ?>
                    </p>
                </div>
            </div>

            <div class="my-1">
                <div class="card card-body p-2">
                    <p class="m-0"><strong>Controller</strong> <?= AbstractTemplate::renderCondition($controller, $controller, 'Aucun associé')?></p>
                </div>
            </div>
            <div class="my-1">
                <div class="card card-body p-2">
                    <p class="m-0"><strong>Route</strong> /<?= AbstractTemplate::renderCondition($route->getRegexSlug(), $route->getRegexSlug(), 'Aucun associé')?></p>
                </div>
            </div>
            <div class="my-1">
                <div class="card card-body p-2">
                    <p class="m-0"><strong>Mémoire max utilisée</strong> <?= round(memory_get_peak_usage()/1024); ?> ko</p>
                </div>
            </div>
            <div class="my-1">
                <div class="card card-body pt-0" style="overflow-y: scroll; max-height: 30vh;">
                    <div class="header position-sticky bg-white border-bottom border-secondary py-2" style="top: 0;">
                        <p class="m-0">Optimisation SQL</p>
                        <p class="m-0">
                            <span class="badge"><?= count(SqlPerfTest::getQueries());?></span>
                            requêtes exécutées en
                            <span class="badge"><?= SqlPerfTest::getTotalExecutionTime(); ?>ms</span>
                        </p>
                    </div>
                    <table>
                        <tr>
                            <th>Temps</th>
                            <th>Table</th>
                            <th>Source</th>
                            <th>+</th>
                        </tr>
                        <?php foreach (SqlPerfTest::getQueries() as $query): ?>
                            <tr>
                                <td>
                                    <span class="badge"><?= $query['exec']; ?>ms</span>
                                </td>
                                <td>
                                    <span class="badge"><?= $query['table']; ?></span>
                                </td>
                                <td>
                                    <span class="badge"><?=  sprintf('%s::%s',$query['source'], $query['methodName']);?></span>
                                </td>
                                <td>
                                    <p class="badge mb-0" onclick="console.log(this.parentElement);
                                    this.parentElement.querySelector('.request').classList.toggle('hidden');">Voir plus
                                    </p>
                                    <div class="request hidden" style="max-width: 30vw; overflow-x: scroll;">
                                        <p class="mb-0">Requête</p>
                                        <p class="badge py-3 mb-0">
                                            <?php
                                            if($query['additionalInfos']['request']):
                                                echo ($query['additionalInfos']['request']);
                                            else:
                                                echo 'aucune info supplémentaire';
                                            endif;
                                            ?>
                                        </p>
                                        <p class="mb-0">Condition</p>
                                        <p class="badge py-3 mb-0">
                                            <?php
                                            if($query['additionalInfos']['request']):
                                                echo ($query['additionalInfos']['condition']);
                                            else:
                                                echo 'aucune info supplémentaire';
                                            endif;
                                            ?>
                                        </p>
                                    </div>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                    </table>
                </div>
            </div>
            <?php if(ErrorCatcher::getErrors()):?>
                <div class="my-1">
                    <div class="card card-body ">
                        <p class="m-0 text-danger font-weight-bold"><?= count(ErrorCatcher::getErrors());?> Erreurs</p>
                        <section style="max-height: 15vh; overflow-y: scroll;">
                            <?php foreach (ErrorCatcher::getErrors() as $i => $error): ?>
                                <div class="my-1">
                                    <p class="m-0 badge text-left"><?= ErrorCatcher::generateClassCallErrorMessage($i); ?></p>
                                    <p class="m-0 badge d-block text-left"><?= ErrorCatcher::getErrorTraceMessage($error, 'last');?></p>
                                </div>
                            <?php endforeach; ?>
                        </section>
                    </div>
                </div>
            <?php endif; ?>
            <div class="my-1">
                <div class="card card-body">
                    <p class="mb-0">
                        <span class="badge"><?= SimpleDataPerfTest::getData('object_autoload_stored')['value']; ?> </span>
                        Objets chargés grâce à l'autoload
                    </p>
                    <p class="mb-0">
                        <span class="badge"><?= SimpleDataPerfTest::getData('dynamic_cache_object')['value']; ?> </span>
                        Résultats chargés grâce au cache avancé dynamique
                    </p>
                    <p class="mb-0">
                        <span class="badge"><?= SimpleDataPerfTest::getData('object_autoload_request')['value']; ?> </span>
                        Requêtes effectuées grâce à l'autoload
                    </p>
                    <p class="mb-0">
                        <span class="badge">
                        <?php
                        $total = SimpleDataPerfTest::getData('object_autoload_stored')['value'] + SimpleDataPerfTest::getData('object_autoload_request')['value'];
                        if(!$total) { $total = 1; }
                        echo round(SimpleDataPerfTest::getData('object_autoload_stored')['value']/$total*100)
                        ?>%
                            </span>Requêtes évitées par l'autoload
                    </p>
                    <p class="mb-0">
                        <span class="badge"><?= SimpleDataPerfTest::getData('rule_cache')['value']; ?> </span>
                        Règles récupérées grâce au cache dynamique
                    </p>
                </div>
            </div>
        </section>



        <?php
    }

}