<?php
namespace Project\Form;

use Core\CoreConnexionManager;
use Project\Core\BaseObject;
use Project\Models\AbstractDbClass;
use Project\Security\ConnexionManager;
use Project\Templates\AdvancedFormTemplate;

abstract class AdvancedAbstractForm {
    private $formId;

    private $schema = [];
    private $datas = [];
    private $mode = 'POST';
    private $prefix = false;
    private $table = false;

    private $formToken = null;
    private $submited = false;
    private $hasSubmitControl = false;
    private $flushAfterProcess = false;

    private $AttachedObject;

    public function __construct()
    {
        $this->setFormId(CoreConnexionManager::generateToken());
    }

    protected function initializeToken()
    {
        $token = CoreConnexionManager::generateToken();
        $this->setFormToken($token);

        $this->addSchema('submit','string','hidden', ['default' => $token]);
    }

    /**
     * Define a basic way to do. So it can be overrided !
     * @param bool $attachedObject
     * @return bool
     */
    protected function validation($attachedObject = false)
    {
        $AttachedObject = $this->getAttachedObject();
        if(!$AttachedObject) { return false; }
        if(!$AttachedObject instanceof AbstractDbClass) { return false; }

        $this->bindAttachedObjectToParams();

        if(!$AttachedObject->isComplete()) { return false; }
        return $AttachedObject->save();
    }

    public function checkValidity(): bool
    {
        if(!$this->isSubmited()) { return false; }
        foreach ($this->getSchema() as $i => $schema) {
            if(!isset($schema['params']['required'])) { continue; }
            if(!$schema['params']['required']) { continue; }

            if($schema['value'] === '' || $schema['value'] === false) { return false; }
        }
        return true;
    }

    public function process($flush = true)
    {
        /**@Todo : Find a way to bind token to validation to prevent multi validation with old token **/
        /*if($this->getFormToken() && $this->getFormToken() !== $this->getSchemaValue('token')) {
            $this->flushFormIfNeeded();
            return false;
        }*/

        if(!$this->bindDataSchema()) { return false; }
        if($this->validation()) {
            if(!$flush) { return true; }
            $this->flushFormIfNeeded();

            if(!$this->hasSubmitControl) { return true; }
            return true;
        }
        return false;
    }

    protected function flushFormIfNeeded()
    {
        if(!$this->isFlushAfterProcess()) { return false; }

        unset($_REQUEST[$this->getPrefix()]);
        $this->setDatas([]);
        $AttachedObjectClass = $this->getAttachedObject()::getClassCompleteName();
        if(class_exists($AttachedObjectClass)) {
            $this->setAttachedObject(new $AttachedObjectClass());
        }
        $this->bindDataSchema();
        $this->bindAttachedObjectToParams();
    }

    protected function addSubmitFieldSchema()
    {
        $this->hasSubmitControl = true;
        $this->addSchema('submit','integer','hidden', ['default' => 1]);
    }

    public function constructDataFilterType($schemaElement, $name)
    {
        $tablePrefix = '';
        if($this->getTable()) {
            $tablePrefix = $this->getTable().'.';
        }

        if($schemaElement['filterValue'] === 'string') {
            return sprintf('%s%s LIKE "%%%s%%"', $tablePrefix, $name, $schemaElement['value']);
        }
        if($schemaElement['filterValue'] === 'integer') {
            return sprintf('%s%s = "%d"', $tablePrefix, $name, $schemaElement['value']);
        }
        if($schemaElement['filterValue'] === 'float') {
            return  sprintf('%s%s = "%f"', $tablePrefix, $name, $schemaElement['value']);
        }
    }
    public function constructSchemaDataFilter($key)
    {
        $schemaElement = $this->getSchema($key);
        if(!$schemaElement) { return false; }

        if($schemaElement['value'] === null) { return false; }
        if($schemaElement['value'] === '') { return false; }
        if($schemaElement['value'] === false) { return false; }

        $condition = [];
        $condition[] = $this->constructDataFilterType($schemaElement, $schemaElement['name']);

        if(isset($schemaElement['params']['secondaryFields'])) {
            foreach ($schemaElement['params']['secondaryFields'] as $secondaryField) {
                $condition[] = $this->constructDataFilterType($schemaElement, $secondaryField);
            }
        }

        $buildedCondition = '('.implode(' OR ', $condition).')';
        return $buildedCondition;
    }

    public function buildFilters()
    {
        $filters = [];
        foreach ($this->getSchema() as $element) {
            $filter = $this->constructSchemaDataFilter($element['name']);
            if(!$filter) { continue; }

            $filters[] = $filter;
        }
        return $filters;
    }

    public function renderFormElement($key, $params = [])
    {
        $schemaElement = $this->getSchema($key);
        if(!$schemaElement) { return; }

        AdvancedFormTemplate::renderElement($schemaElement, $params);
    }

    protected function bindAttachedObjectToParams()
    {
        if(!$this->getAttachedObject()) { return false; }
        $this->getAttachedObject()->bindParametersToObject($this->extractSchemaToParams());
    }

    /**
     *
     */
    public function getSchema($key = false)
    {
        if(!$key) { return $this->schema; }
        if(!isset($this->schema[$key])) { return false; }
        return $this->schema[$key];
    }

    public function setSchema($schema)
    {
        $this->schema = $schema;
        return $this;
    }

    public function updateSchemaParams($key, $params = [])
    {
        $schema = $this->getSchema($key);
        if(!$schema) { return false; }

        foreach ($params as $index => $value) { $schema['params'][$index] = $value; }

        $this->schema[$key]['params'] = $schema['params'];
    }

    public function updateSchemaValue($key, $value)
    {
        $schema = $this->getSchema($key);
        if(!$schema) { return false; }
        $this->schema[$key]['value'] = $value;
    }
    public function updateSchemaDefault($key, $value)
    {
        $schema = $this->getSchema($key);
        if(!$schema) { return false; }
        $this->schema[$key]['params']['default'] = $value;
    }

    public function extractSchemaToParams()
    {
        $params = [];
        foreach ($this->getSchema() as $i => $schema) {
            if($schema['value'] == '') { continue; }
            $params[$i] = $schema['value'];
        }
        return $params;
    }

    public function getSchemaValue($key, $strict = false)
    {
        $schema = $this->getSchema($key);
        if(!$schema) { return false; }

        $default = false;
        if(isset($schema['params']['default'])) { $default = $schema['params']['default']; }

        if($strict) { return $schema['value']; }
        return $schema['value'] ?: $default;
    }

    public function addSchema($key, $filterValue = 'string', $type = 'text', $params = [])
    {
        if(!isset($params['label'])) { $params['label'] = false; }
        if(!isset($params['required'])) { $params['required'] = false; }

        if(!isset($params['classes'])) { $params['classes'] = []; }
        if(!isset($params['attributes'])) { $params['attributes'] = []; }

        if(!is_array($params['classes'])) { $classes = [$params['classes']]; }
        $params['classes'] = implode(' ', $params['classes']);

        $completeName = $key;
        $value = ConnexionManager::getRequest($key);
        if($this->getPrefix()) {
            $completeName = sprintf('%s[%s]', $this->getPrefix(), $key);
            $value = '';
            if(isset(ConnexionManager::getRequest($this->getPrefix())[$key])) {
                $value = ConnexionManager::getRequest($this->getPrefix())[$key];
            }
        }

        if($type == 'number' && $filterValue == 'float') { $params['attributes']['step'] = 0.01; }

        $this->schema[$key] = [
            'name' => $key,
            'filterValue' => $filterValue,
            'prefix' => $this->getPrefix(),
            'completeName' => $completeName,
            'value' => $value,
            'type' => $type,
            'params' => $params
        ];

        return $this;
    }

    public function bindDataSchema()
    {
        $datas = [];
        if(!isset($_REQUEST[$this->getPrefix()])) {return false; }

        foreach ($this->getSchema() as $key => $schemaElement) {
            if($this->getPrefix()) {
                $this->updateSchemaValue($key, $_REQUEST[$this->getPrefix()][$key]);
                if (!isset($_REQUEST[$this->getPrefix()][$key])) { continue; }
                $datas[$key] = $_REQUEST[$this->getPrefix()][$key];
                continue;
            }

            $this->updateSchemaValue($key, $_REQUEST[$key]);
            if (!isset($_REQUEST[$key])) { continue; }
            $datas[$key] = $_REQUEST[$key];
        }

        if($this->hasSubmitControl && isset($datas['submit']) && $datas['submit']) {
            $this->setSubmited(true);
        }

        $this->setDatas($datas);
        if(!$this->checkValidity($datas)) { return false; }

        return true;
    }

    /**
     * @return mixed
     */
    public function getFormId()
    {
        return $this->formId;
    }

    /**
     * @param mixed $formId
     * @return AdvancedAbstractForm
     */
    public function setFormId($formId)
    {
        $this->formId = $formId;
        return $this;
    }


    /**
     * @return array
     */
    public function getDatas(): array
    {
        return $this->datas;
    }

    public function getData($key)
    {
        if(!isset($this->getDatas()[$key])) { return false; }
        return $this->getDatas()[$key];
    }

    /**
     * @param array $datas
     */
    public function setDatas(array $datas): void
    {
        $this->datas = $datas;
    }



    /**
     *
     */
    public function getPrefix()
    {
        return $this->prefix;
    }

    /**
     * @return AdvancedAbstractForm
     */
    public function setPrefix($prefix): AdvancedAbstractForm
    {
        $this->prefix = $prefix;
        return $this;
    }

    /**
     * @return string
     */
    public function getMode(): string
    {
        return $this->mode;
    }

    /**
     * @return bool
     */
    public function getTable()
    {
        return $this->table;
    }

    /**
     * @param mixed $table
     * @return AdvancedAbstractForm
     */
    public function setTable($table): AdvancedAbstractForm
    {
        $this->table = $table;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getFormToken()
    {
        return $this->formToken;
    }

    /**
     * @param string|null $formToken
     * @return AdvancedAbstractForm
     */
    public function setFormToken($formToken): AdvancedAbstractForm
    {
        $this->formToken = $formToken;
        return $this;
    }

    /**
     * @return bool
     */
    public function isSubmited(): bool
    {
        return $this->submited;
    }

    /**
     * @param bool $submited
     * @return AdvancedAbstractForm
     */
    public function setSubmited(bool $submited): AdvancedAbstractForm
    {
        $this->submited = $submited;
        return $this;
    }

    /**
     * @return BaseObject
     */
    public function getAttachedObject()
    {
        return $this->AttachedObject;
    }

    /**
     * @param mixed $AttachedObject
     * @return AdvancedAbstractForm
     */
    public function setAttachedObject($AttachedObject)
    {
        $this->AttachedObject = $AttachedObject;
        return $this;
    }

    /**
     * @return bool
     */
    public function isFlushAfterProcess(): bool
    {
        return $this->flushAfterProcess;
    }

    /**
     * @param bool $flushAfterProcess
     * @return AdvancedAbstractForm
     */
    public function setFlushAfterProcess(bool $flushAfterProcess): AdvancedAbstractForm
    {
        $this->flushAfterProcess = $flushAfterProcess;
        return $this;
    }
}