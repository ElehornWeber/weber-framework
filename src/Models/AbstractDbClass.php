<?php
namespace Project\Models;

use Project\Core\BaseObject;
use Project\Dao\AbstractDao;
use Project\Dao\Builder\AbstractBuilderElement;
use Project\Dao\Builder\Condition\Single;
use Project\Dao\Builder\CoreBuilder;
use Project\Dao\DynamicCache;
use Project\Security\ConnexionManager;
use Project\Utilities\SimpleDataPerfTest;
use Project\Utilities\SimpleTimePerfTest;
use ReflectionProperty;

abstract class AbstractDbClass extends BaseObject {
    private $allowedProperties = [];
    private $uniqueIdProperty = 'id';
    private $id;
    private $tableName = null;
    private $databaseName = '';

    public function save()
    {
        if(!$this->isComplete()) { return false; }
        if($this->getUniquePropertyValue()) {
            $this::getDao()->update($this);
            return true;
        }
        $this::getDao()->create($this);
        return true;
    }

    public function delete()
    {
        if(!$this->isDeleteAllowed()) { return false; }
        $this::getDao()->delete($this);
        return true;
    }

    protected function isDeleteAllowed(): bool
    {
        return true;
    }

    abstract public function isComplete(): bool;

    static public function getDao()
    {
        $className = static::class;
        /**@var AbstractDbClass $Model */
        $Model = new $className();
        $DaoGroup = ConnexionManager::getDaoCollection()->getGroupByDatabase($Model->getDatabaseName());

        $Dao = $DaoGroup->findDaoFromClass($Model);
        if(!$Dao) { return new AbstractDao(); }
        return $Dao;
    }

    /**
     * @param $targetClass => target class to load
     * @param $sourceProperty => orignPropertyValue
     * @param null $forcedAutoload => should we autoload ?
     * @param null $sourcePropertyName => name of $sourceProperty, used to store the loaded result
     * @param bool $distProperty => dist property for the condition, default is 'id'
     * @param string $oneTo => 'one' or 'many'
     * @param array $parameters => CoreBuilder elements
     * @param mixed $initialValue => to create reversed relation
     * @return mixed
     */
    public function autoLoadObject(
        $targetClass,
        $sourceProperty,
        $forcedAutoload = null,
        $sourcePropertyName = null,
        $distProperty = false,
        $oneTo = 'one',
        $parameters = [],
        $initialValue = null
    )
    {
        $perf = new SimpleTimePerfTest();

        /**@var AbstractDbClass $distModel*/
        $distModel = new $targetClass();

        if(!$distProperty) { $distProperty = $distModel->getUniqueIdProperty(); }
        $distProperty = $distModel->getFullPropertyName($distProperty);

        $SourcePropertyReflection = false;
        try {
            $SourcePropertyReflection = new ReflectionProperty($this, $sourcePropertyName);
        } catch (\ReflectionException $e) { }

        $sourcePropertyValue = $sourceProperty;

        if($SourcePropertyReflection && !$SourcePropertyReflection->isPrivate()) {
            if($this->$sourcePropertyName) {
                $sourcePropertyValue = $this->$sourcePropertyName;
            }
        }

        if ($sourcePropertyValue instanceof AbstractDbClass || (is_array($sourcePropertyValue) && count($sourcePropertyValue) > 0)) {
            SimpleDataPerfTest::incrementData('autoloading_time', $perf->getTestResult());
            if (!$forcedAutoload && !is_array($sourcePropertyValue)) {
                return $sourcePropertyValue->getUniquePropertyValue();
            }
            return $sourcePropertyValue;
        } else if(($sourcePropertyValue === null || $sourcePropertyValue === []) && $initialValue === null) {
            SimpleDataPerfTest::incrementData('autoloading_time', $perf->getTestResult());
            if($oneTo === 'one') { return false; }
            return [];
        }

        $oldAutoload = $this->isAutoloadObjects();
        if($forcedAutoload !== null) { $this->setAutoloadObjects($forcedAutoload); }

        if(!$this->isAutoloadObjects()) {
            $this->setAutoloadObjects($oldAutoload);
            SimpleDataPerfTest::incrementData('autoloading_time', $perf->getTestResult());
            return $sourcePropertyValue;
        }

        /** We will now need to load the targeted object **/

        if($sourcePropertyValue === false) { $sourcePropertyValue = 0; }

        $targetClass = new \ReflectionClass($targetClass);
        /**@var AbstractDbClass $TargetModel*/
        $TargetModel = new $targetClass->name();
        $className = $TargetModel::getClassShortName();

        $Builder = new CoreBuilder();
        $Builder->setTableName($TargetModel->getTableName());
        $Builder->setIsSingle(true);
        $Builder->setMethodName('Autoload');
        if($oneTo === 'many') { $Builder->setIsSingle(false); }

        $conditionTarget = $sourcePropertyValue;
        if($initialValue !== null && !$sourcePropertyValue) { $conditionTarget = $initialValue; }

        $Builder->addToBuilder(new Single($distProperty, $conditionTarget));

        foreach ($parameters as $type => $builderElement) {
            if($builderElement instanceof AbstractBuilderElement) { $Builder->addToBuilder($builderElement); }
        }

        $result = DynamicCache::getResultsFromAdvancedCache($targetClass->getName(), $Builder);

        $dataPerfKey = 'object_autoload_cached';
        if(!$result) {
            $result = $TargetModel::getDao()->get($Builder);
            $dataPerfKey = 'object_autoload_request';
        }

        $insertMethod = $this->formatPropertyToMethod($sourcePropertyName, 'set');
        if(method_exists($this, $insertMethod)) {
            $this->$insertMethod($result);
        } else if(method_exists($this, $this->formatPropertyToMethod($sourcePropertyName, 'set'))) {
            $this->formatPropertyToMethod($sourcePropertyName, 'set')($result);
        }

        $this->setAutoloadObjects($oldAutoload);

        SimpleDataPerfTest::incrementData($dataPerfKey, $perf->getTestResult());

        return $result;
    }

    protected function addAllowedProperty($key, $property)
    {
        if (!property_exists($this, $property)) { return false; }
        if($this->isPropertyProtected($property)) { return false; }

        $this->allowedProperties[$key][] = $property;
    }

    protected function addAllowedProperties($key, $props)
    {
        if(!is_array($props)) { $props = [$props]; }
        foreach($props as $property) {
            $this->addAllowedProperty($key, $property);
        }
    }

    public function getAllowedProperties($key)
    {
        if(isset($this->allowedProperties[$key])) { return $this->allowedProperties[$key];}
        return [];
    }

    public function __toString(): string
    {
        return $this->getUniquePropertyValue() ?: '';
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     * @return AbstractDbClass
     */
    public function setId($id)
    {
        $this->id = intval($id);
        return $this;
    }


    public function getUniquePropertyValue()
    {
        return $this->dynamicPropertyAccess($this->getUniqueIdProperty(), 'get');
    }

    public function setUniquePropertyValue($value = null)
    {
        return $this->dynamicPropertyAccess($this->getUniqueIdProperty(), 'set', $value);
    }

    /**
     * @return string
     */
    public function getUniqueIdProperty(): string
    {
        return $this->uniqueIdProperty;
    }

    /**
     * @param string $uniqueIdProperty
     * @return AbstractDbClass
     */
    public function setUniqueIdProperty(string $uniqueIdProperty): AbstractDbClass
    {
        $this->uniqueIdProperty = $uniqueIdProperty;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getTableName()
    {
        return $this->tableName;
    }

    /**
     * @param mixed $tableName
     * @return AbstractDbClass
     */
    public function setTableName($tableName = '')
    {
        $this->tableName = $tableName;
        return $this;
    }

    /**
     * @return string
     */
    public function getDatabaseName(): string
    {
        return $this->databaseName;
    }

    /**
     * @param string $databaseName
     * @return AbstractDbClass
     */
    public function setDatabaseName(string $databaseName = ''): AbstractDbClass
    {
        $this->databaseName = $databaseName;
        return $this;
    }

    static public function getBaseNameSpace()
    {
        return __NAMESPACE__;
    }
}
