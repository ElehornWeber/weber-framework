<?php
namespace Core;


use Project\Dao\DaoGroupCollection;
use Project\PageManager;
use Project\Utilities\FlashSessionMessage;
use Project\Security\Security;

abstract class CoreConnexionManager
{
    protected static $Security = null;
    protected static $sessionToken = '';
    protected static $DaoCollection = null;

    static public function initializeConnexion()
    {
        static::$DaoCollection = new DaoGroupCollection();
    }

    static public function getDaoCollection() : DaoGroupCollection
    {
        if(!static::$DaoCollection) { static::initializeConnexion(); }
        return static::$DaoCollection;
    }

    static public function getSecurity(): Security
    {
        if(!static::$Security) { static::$Security = new Security(); }
        return static::$Security;
    }

    static public function isConnected()
    {
        return static::getSecurity()->checkSecurity('IS_CONNECTED');
    }

    static public function showFlashSessionMessages()
    {
        $messages = static::getFromSession('flashSessionMessage');
        if(!$messages) { return false; }

        /** @var FlashSessionMessage $message */
        foreach ($messages as $i => $message) {
            if($message->report === 0) {
                echo $message->generateFlashMessage();
                unset($messages[$i]);
                continue;
            }
            $message->report--;
        }
        static::setToSession('flashSessionMessage', $messages);
    }

    static public function addFlashSessionMessage($type, $message, $report = 0)
    {
        if(!class_exists('Project\Utilities\FlashSessionMessage')) { return false; }

        $flashSession = new FlashSessionMessage();
        $flashSession->message = $message;
        $flashSession->type = $type;
        $flashSession->report = $report;

        if(!isset($_SESSION['flashSessionMessage']) || !is_array($_SESSION['flashSessionMessage'])) {
            static::setToSession('flashSessionMessage', []);
        }
        $_SESSION[static::$sessionToken]['flashSessionMessage'][] = $flashSession;
    }

    static public function generateToken(): string
    {
        return bin2hex(random_bytes(10));
    }

    static public function getRequest($key) {
        if(!isset($_REQUEST[$key])) { return false; }
        return $_REQUEST[$key];
    }

    static public function setRequest($key, $value) {
        $_REQUEST[$key] = $value;
    }

    static public function setToSession($key, $value)
    {
        if(!static::getSessionToken()) { static::loadSessionToken(); }

        $_SESSION[static::$sessionToken][$key] = $value;
    }

    static public function unsetFromSession($key)
    {
        if(!static::getSessionToken()) { static::loadSessionToken(); }

        if(!isset($_SESSION[static::$sessionToken][$key])) { return false; }
        unset($_SESSION[static::$sessionToken][$key]);
    }

    static public function getFromSession($key)
    {
        if(!static::getSessionToken()) { static::loadSessionToken(); }

        if(isset($_SESSION[static::$sessionToken][$key])) {
            return $_SESSION[static::$sessionToken][$key];
        }
    }

    /**
     * @return string
     */
    public static function getSessionToken()
    {
        return static::$sessionToken;
    }

    private static function loadSessionToken()
    {
        $token = bin2hex(PageManager::getProjectAbsoluteRoot());
        static::$sessionToken = $token;

        static::initializeSessionPart();
    }

    public static function initializeSessionPart()
    {
        if(!isset($_SESSION[static::$sessionToken])) {
            $_SESSION[static::$sessionToken] = [];
        }
    }

    public static function flushSession()
    {
        unset($_SESSION[static::$sessionToken]);
        static::initializeSessionPart();
    }

}