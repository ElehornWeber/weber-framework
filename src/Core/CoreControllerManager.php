<?php
namespace Project\Core;

use Project\Utilities\DataConverter;
use Project\Utilities\DirectoryManipulator;

abstract class CoreControllerManager
{
    protected $controllers = [];

    private static $baseNameSpace = 'Project\\Controllers';
    private static $basePath = '../src/Controllers';

    public function addController($ControllerClass, $key, $arguments = [], $route = ''): bool
    {
        if(!class_exists($ControllerClass)) { return false; }
        /* @var CoreController $newController */
        $newController = new $ControllerClass();
        $newController->setKey($key);
        $newController->setArguments($arguments);
        $newController->setRoute($route);

        if(isset($this->controllers[$key])) { return false; }

        $this->controllers[$key] = $newController;

        return true;
    }

    public function getProcess($Controller, $method = 'process')
    {
        if(!$Controller) { return false; }
        if(!$Controller instanceof CoreController) { return false; }

        $Controller->process($method);
    }

    public function findControllerByClass($class)
    {
        if(!class_exists($class)) { return false; }

        $instance = new $class();
        if($instance && $instance instanceof CoreController) {
            $this->addController($class, $instance->getKey(), $instance->getArguments(), $instance->getRoute());
            return $instance;
        }

        return false;
    }

    public function findController($key)
    {
        if($this->isControllerExists($key)) { return $this->getController($key); }

        $controller = $this->findControllerByClass($key);
        if($controller) { return $controller; }

        $parsedKey = DataConverter::ConvertToCamelCase($key);
        $baseDirectory = DirectoryManipulator::getDirectory(static::getBasePath());

        foreach ($baseDirectory as $subDir) {
            if(str_ends_with($subDir, '.php')) { continue; }
            $ScopedNameSpace = sprintf('\%s\%s\%s', static::getBaseNameSpace(), $subDir, $parsedKey);
            if(!class_exists($ScopedNameSpace) && class_exists($ScopedNameSpace.'Controller')) {
                $ScopedNameSpace .= 'Controller';
            }

            $class = false;
            if(class_exists($ScopedNameSpace)) { $class = $ScopedNameSpace; }
            if(!$class) { continue; }

            $instance = new $class();
            if($instance && $instance instanceof CoreController) {
                $this->addController($class, $instance->getKey(), $instance->getArguments(), $instance->getRoute());
                return $instance;
            }
        }

        return false;
    }

    /**
     * @return array
     */
    public function getControllers(): array
    {
        return $this->controllers;
    }

    public function isControllerExists($key): bool
    {
        $Controller = $this->getController($key);
        if(!$Controller) { return false; }

        return true;
    }
    public function getController($key)
    {
        if(!isset($this->controllers[$key])) { return false; }
        return $this->controllers[$key];
    }


    /**
     * @param array $controllers
     * @return CoreControllerManager
     */
    public function setControllers(array $controllers): CoreControllerManager
    {
        $this->controllers = $controllers;
        return $this;
    }

    /**
     * @return string
     */
    public static function getBaseNameSpace(): string
    {
        return self::$baseNameSpace;
    }

    /**
     * @param string $baseNameSpace
     */
    public static function setBaseNameSpace(string $baseNameSpace): void
    {
        self::$baseNameSpace = $baseNameSpace;
    }

    /**
     * @return string
     */
    public static function getBasePath(): string
    {
        return self::$basePath;
    }

    /**
     * @param string $basePath
     */
    public static function setBasePath(string $basePath): void
    {
        self::$basePath = $basePath;
    }

}