<?php
namespace Project\Core;


use Core\CoreRouter;

abstract class CoreApi extends CoreController
{
    protected static $apiRoutes = [];
    abstract public static function setApiRoutes();

    protected static function addApiRoute($slug, $method, $parameters = [], $securityLevel = ''): bool
    {
        if(!method_exists(static::class, $method)) { return false; }

        $parameters['isApi'] = true;

        static::$apiRoutes[$slug] = [
            'method' => $method,
            'parameters' => $parameters,
            'securityLevel' => $securityLevel
        ];
        return true;
    }

    public static function createApi(CoreRouter $Router)
    {
        static::setApiRoutes();

        foreach (static::$apiRoutes as $slug => $routeData) {
            $Router->addRoute(sprintf('%s:%s',static::class, $routeData['method']), $slug, $routeData['securityLevel'], $routeData['parameters']);
        }
        static::$apiRoutes = [];
    }


    protected function action() { }
}