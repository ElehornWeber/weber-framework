<?php


namespace Core;


abstract class AbstractMap
{
    static protected $map = [];
    static protected $alias = [];

    static protected $mapType = 'string';

    public static function getValue(string $key)
    {
        if(isset(static::$map[$key])) { return static::$map[$key]; }
        if(isset(static::$alias[$key])) { return static::getValue(static::$alias[$key]); }
        return static::emptyValueReturn();
    }

    protected static function emptyValueReturn()
    {
        switch (static::$mapType) {
            case 'string': return ''; break;
            case 'integer': case 'float':  return 0; break;
            case 'array': return []; break;
            case 'boolean': default: return false; break;
        }
    }
}