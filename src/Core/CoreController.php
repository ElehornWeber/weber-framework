<?php
namespace Project\Core;

use Project\Security\ConnexionManager;
use Project\Security\HttpResponse;
use Project\Security\Route;

abstract class CoreController
{
    /* must be unique */
    private $key = null;

    private $arguments = [];
    protected $parameters = [];
    private $route = null;

    public function notAllowed()
    {
        return HttpResponse::createHttpResponse(500, ["error" => "not_allowed"]);
    }

    public function notFound()
    {
        return HttpResponse::createHttpResponse(404, ["error" => "route_not_found"]);
    }

    protected abstract function action();

    protected function error()
    {

    }


    public function bindRoute(Route $route)
    {
        $this->setRoute($route);
        $this->setArguments($route->getArguments());
        $this->setParameters($route->extractParameters());
    }

    public function process($method = false)
    {
        if($this->getRoute()) { $method = $this->getRoute()->getMethod(); }

        if($method !== 'process' && method_exists($this, $method)) {
            $this->$method();
            return;
        }
        $this->action();
    }

    /**
     * @return null
     */
    public function getKey()
    {
        return $this->key;
    }

    /**
     * @param null $key
     * @return CoreController
     */
    public function setKey($key)
    {
        $this->key = $key;
        return $this;
    }

    /**
     * @return array
     */
    public function getArguments(): array
    {
        return $this->arguments;
    }

    /**
     * @param array $arguments
     * @return CoreController
     */
    public function setArguments(array $arguments): CoreController
    {
        $this->arguments = $arguments;
        return $this;
    }

    /**
     * @return null
     */
    public function getRoute()
    {
        return $this->route;
    }

    /**
     * @param null $route
     * @return CoreController
     */
    public function setRoute($route)
    {
        $this->route = $route;
        return $this;
    }

    /**
     * @return array
     */
    public function getParameters(): array
    {
        return $this->parameters;
    }

    public function getParameter($key)
    {
        if(!isset($this->parameters[$key])) { return null; }

        return $this->parameters[$key];
    }

    public function addParameter($key, $value)
    {
        $this->parameters[$key] = $value;
    }

    /**
     * @param array $parameters
     * @return CoreController
     */
    public function setParameters(array $parameters): CoreController
    {
        $this->parameters = $parameters;
        return $this;
    }
}