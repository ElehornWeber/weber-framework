<?php
namespace Core;

use Project\Core\CoreApi;
use Project\Core\CoreController;
use Project\PageManager;
use Project\Security\Route;
use Project\Utilities\DirectoryManipulator;

abstract class CoreRouter
{
    protected $fileReferences = [];
    protected $slugReferences = [];

    protected $autoCreateRoutes = false;

    public function __construct()
    {
        $this->autoCreateRoutes();

        $this->initializeRoutes();
    }

    protected function autoCreateRoutes()
    {
        if(!$this->autoCreateRoutes) { return false; }
        $controllers = $this->getControllers();

        foreach ($controllers as $controller) {
            if($controller instanceof CoreApi) { $controller::createApi($this); }
        }
    }

    protected function getControllers($dir = false)
    {
        $controllers = [];

        $baseDir = sprintf('%s/src/Controllers', PageManager::getDirectoryAbsoluteRoot());
        if(!$dir) { $dir = $baseDir; }
        $directoryContent = DirectoryManipulator::getDirectory($dir);
        foreach ($directoryContent as $file) {
            $target = sprintf('%s/%s', $dir, $file);
            if(is_dir($target)) {
                $controllers += $this->getControllers($target);
                continue;
            }
            $target = str_replace($baseDir, "", $target);
            $target = str_replace("/", "\\", $target);
            $target = str_replace(".php", "", $target);


            $baseNamespaces = ['Project\Controllers', 'Project\Controllers\Api', 'Project'];
            $controller = false;
            foreach ($baseNamespaces as $baseNamespace) {
                $namespace = $baseNamespace.$target;
                if(class_exists($namespace)) {
                    $controller = new $namespace();
                    break;
                }
            }

            if(isset($controllers[$namespace])) { continue; }
            if(!$controller) { continue; }
            if(!$controller instanceof CoreController) { continue; }

            $controllers[$namespace] = $controller;
        }

        return $controllers;
    }

    abstract protected function initializeRoutes();


    /**
     * @param string $controller
     * @return false | Route
     */
    public function getRouteByController(string $controller)
    {
        if(!isset($this->fileReferences[$controller])) { return false; }
        return $this->fileReferences[$controller];
    }


    public function bindRouteParameters(Route $route): bool
    {
        $parameters = $route->extractParameters();
        if(!$parameters) { return false; }
        foreach ($parameters as $key => $parameter) { $_REQUEST[$key] = $parameter; }

        return true;
    }

    /**
     * Work in 2 steps
     * Step 1 :
     * We try a basic research like $path is a key of Router->fileReferences || Router->slugRerences.
     * If there's no Route defined, go to Step 2 =>
     * Step 2:
     * Loop on slugReferences. Test each key as a regex. If regex match $path, return the current Route.
     *
     * If Step 1 and Step 2 failed, return empty Route Object
     * => Maybe return 404 : Route not found ?
     *
     * @param $path
     * @return Route
     */
    public function getRoute($path): Route
    {
        /*
         * Step 1 : Basic search
         */
        if(isset($this->slugReferences[$path])) { return $this->slugReferences[$path]; }
        if(isset($this->fileReferences[$path])) { return $this->fileReferences[$path]; }

        /*
         * Step 2 : Complexe research
         */

        /** @var Route $route */
        foreach ($this->slugReferences as $key => $route) {
            $escapedFile = str_replace('\\', '\\\\', $route->getRegexSlug());
            $regex = preg_match(sprintf('@^%s$@', $escapedFile), $path);
            if(!$regex) { continue; }
            return $route;
        }

        foreach ($this->fileReferences as $key => $route) {
            $escapedFile = str_replace('\\', '\\\\', $route->getFile());
            $regex = preg_match(sprintf('@^%s$@', $escapedFile), $path);
            if(!$regex) { continue; }

            return $route;
        }

        if($path === '/') { return $this->getRoute(''); }
        return new Route();
    }

    public function getCurrentRoute(): Route
    {
        return $this->getRoute(PageManager::getPage());
    }

    public function addRoute($file, $slug, $securityLevel = 1, $arguments = [])
    {
        $fileMethod = explode(':', $file);
        $Route = new Route();
        $Route
            ->setFile($fileMethod[0])
            ->setSlug($slug)
            ->setSlug($Route->getRegexSlug())
            ->setSecurityLevel($securityLevel);

        if(isset($fileMethod[1])) {
            $Route->setMethod($fileMethod[1]);
        }
        if(isset($arguments['method'])) {
            $Route->setMethod($arguments['method']);
            unset($arguments['method']);
        }

        $Route->setArguments($arguments);

        $this->fileReferences[$Route->getKey()] = $Route;
        $this->slugReferences[$Route->getSlug([], true)] = $Route;
    }

    /**
     * @return array
     */
    public function getFileReferences(): array
    {
        return $this->fileReferences;
    }

    /**
     * @param array $fileReferences
     * @return CoreRouter
     */
    public function setFileReferences(array $fileReferences): CoreRouter
    {
        $this->fileReferences = $fileReferences;
        return $this;
    }

    /**
     * @return array
     */
    public function getSlugReferences(): array
    {
        return $this->slugReferences;
    }

    /**
     * @param array $slugReferences
     * @return CoreRouter
     */
    public function setSlugReferences(array $slugReferences): CoreRouter
    {
        $this->slugReferences = $slugReferences;
        return $this;
    }
}