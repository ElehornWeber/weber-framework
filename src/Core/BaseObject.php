<?php
namespace Project\Core;

use Project\Models\AbstractDbClass;
use Project\Utilities\ErrorCatcher;
use ReflectionClass;

class BaseObject {

    private $protectedProperties = [];
    private $autoload_objects = true;
    private $multipleName = '';

    private $propertiesNameComplement = [
        'prefix' => [],
        'mapping' => [],
        'suffix' => []
    ];

    public function __call($method, $arguments)
    {
        $error = new \Error();
        ErrorCatcher::addClassErrorMessage($this::getClassShortName(), $method, $arguments, $error);

    }

    public function dynamicPropertyAccess($propertyName, $type = 'get', $value = null)
    {
        $methodName = $this->formatPropertyToMethod($propertyName, $type);
        if(!$methodName && $type === 'get') {
            $methodName = $this->formatPropertyToMethod($propertyName, 'is');
            if(!$methodName) { return ''; }
        }

        $result = $this->$methodName($value);


        if(is_object($result)) {
            if($result instanceof AbstractDbClass) { return $result->getUniquePropertyValue(); }
        }

        return $result;
    }

    public function dynamicGetterByProp($propName)
    {
        return $this->dynamicPropertyAccess($propName, 'get');
    }

    /**
     * Get the properties of an object. ReflectionClass return an array like [ => Object{name: @name, objectOrigin: @Object} ]
     * @return array
     */
    public static function getObjectProperties(): array
    {
        $reflect = new ReflectionClass(static::class);
        return $reflect->getProperties();
    }

    /**
     * @param string $caseType (can be camel or snake)
     * @return array
     */
    public static function getObjectPropertiesKeyed($caseType = 'snake')
    {
        $reflected = static::getObjectProperties();
        $properties = [];
        foreach($reflected as $reflect) {
            //default is snake
            $format = $reflect->name;
            if($caseType == 'camel') {
                $format = str_replace('_', ' ', $reflect->name);
                $format = ucwords($format);
                $format = str_replace(' ','', $format);
            }
            $properties[$format] = $reflect;
        }
        return $properties;
    }

    public function getBasicPropertyName($property)
    {
        foreach ($this->propertiesNameComplement['prefix'] as $prop => $complement) {
            $potentialProperty = str_replace($complement.$prop, $prop, $property);
            if($potentialProperty !== $property) { $property = $potentialProperty; break; }
        }
        foreach ($this->propertiesNameComplement['suffix'] as $prop => $complement) {
            $potentialProperty = str_replace($prop.$complement, $prop, $property);
            if($potentialProperty !== $property) { $property = $potentialProperty; break; }
        }

        return $property;
    }

    public function getFullPropertyName($property)
    {
        return sprintf(
            '%s%s%s',
            $this->getPropertyNameComplement('prefix', $property),
            $property,
            $this->getPropertyNameComplement('suffix', $property)
        );
    }

    public static function getNamespace()
    {
        $reflect = new ReflectionClass(static::class);
        return $reflect->getNamespaceName();
    }

    public static function getClassShortName()
    {
        $reflect = new ReflectionClass(static::class);
        return $reflect->getShortName();
    }

    public static function getClassCompleteName()
    {
        $reflect = new ReflectionClass(static::class);
        return $reflect->getName();
    }

    public function formatPropertyToMethod($property, $method = 'get') {
        $cleanProperty = str_replace('_',' ', $property);
        $cleanProperty = ucwords($cleanProperty);
        $cleanProperty = str_replace(' ','', $cleanProperty);

        $methodName = sprintf('%s%s',$method, $cleanProperty);

        if($method === '') { return $methodName; }

        $methodName = lcfirst($methodName);
        if(!method_exists($this, $methodName)) { return false; }

        return $methodName;
    }

    /**
     * Assign an array [@property => @value] to an object.
     * @param array $array
     * @param string $prefix
     * @return int
     */
    public function bindParametersToObject($array = [], $prefix = '')
    {
        $updatedParameters = 0;

        foreach ( $array as $property => $data) {

            $methodName = $this->formatPropertyToMethod(
                str_replace($prefix,'', $this->getBasicPropertyName($property)),
                'set'
            );
            if(!$methodName) { continue; }

            $this->{$methodName}($data);
            $updatedParameters++;
        }

        return $updatedParameters;
    }

    /**
     * Convert an object to an array like [@property => @value, ...]
     * Is Recursive, if Object has an Object in prop, convert it too.
     * @param string $prefix
     * @param bool $autoload
     * @param bool $includeUniqueProperty
     * @return array
     */
    public function objectToArray($prefix = '', $autoload = true, bool $includeUniqueProperty = true): array
    {
        $result = [];

        foreach (static::getObjectProperties() as $objectProperty) {
            $format = str_replace('_', ' ', $objectProperty->name);
            $format = ucwords($format);
            $cleanProp = str_replace(' ','', $format);

            if($this->isPropertyProtected($cleanProp)) { continue; }

            $propName = sprintf('get%s', $cleanProp);
            $methodName = $this->formatPropertyToMethod($objectProperty->name, 'get');

            if(!method_exists($this, $propName)) { continue; }
            if(!$methodName) { continue; }

            if(is_object($this->{$propName}())) {
                if($autoload) {
                    $result[$objectProperty->name] = $this->{$methodName}()->objectToArray('', false);
                } else {
                    if($this->{$methodName}() instanceof AbstractDbClass) {
                        $result[$objectProperty->name] = $this->{$methodName}()->getUniqueIdProperty();
                    } else {
                        $result[$objectProperty->name] = $this::getClassShortName();
                    }
                }

                continue;
            }

            $result[$objectProperty->name] = $this->{$propName}();
        }

        if($includeUniqueProperty && $this instanceof AbstractDbClass) {
            $result[$this->getUniqueIdProperty()] = $this->getUniquePropertyValue();
        }

        return $result;
    }

    /**
     * @param array $objectArray
     * @param false|string $keyOrdering
     * @param array $converters
     * @example A converter is a $property => function($value, $object) that will convert the data before output.
     * @return array
     */
    static public function MultipleObjectsToArray(array $objectArray = [], $keyOrdering = false, array $converters = []): array
    {
        $response = [];
        foreach ($objectArray as $object) {
            $array = $object->objectToArray();
            if($converters) {
                foreach ($converters as $property => $converter) {
                    if (isset($array[$property])) {
                        $array[$property] = $converter($array[$property], $object);
                    }
                }
            }
            if($keyOrdering && isset($array[$keyOrdering])) {
                $response[$array[$keyOrdering]] = $array;
                continue;
            }
            $response[] = $array;
        }
        return $response;
    }

    /**
     * @return bool
     */
    public function isAutoloadObjects(): bool
    {
        return $this->autoload_objects;
    }

    /**
     * @param bool $autoload_objects
     * @return BaseObject
     */
    public function setAutoloadObjects(bool $autoload_objects): BaseObject
    {
        $this->autoload_objects = $autoload_objects;
        return $this;
    }


    /**
     * @return array
     */
    public function getProtectedProperties(): array
    {
        return $this->protectedProperties;
    }

    public function isPropertyProtected($property): bool
    {
        if(isset($this->protectedProperties[$property])) { return true; }
        return false;
    }

    /**
     * @param array $protectedProperties
     * @return BaseObject
     */
    public function setProtectedProperties(array $protectedProperties): BaseObject
    {
        $this->protectedProperties = $protectedProperties;
        return $this;
    }

    protected function addProtectedProperties($propertyName) {
        $this->protectedProperties[$propertyName] = true;
    }


    public function addPropertyNameComplement($complementType, $target = 'default', $complement = '')
    {
        if(!isset($this->propertiesNameComplement[$complementType])) {
            $this->propertiesNameComplement[$complementType] = [];
        }

        $this->propertiesNameComplement[$complementType][$target] = $complement;
    }
    public function removePropertyNameComplement($complementType, $target = 'default')
    {
        if(!isset($this->propertiesNameComplement[$complementType])) { return false; }
        if(!isset($this->propertiesNameComplement[$complementType][$target])) { return false; }

        unset($this->propertiesNameComplement[$complementType][$target]);
        return true;
    }

    public function getPropertyNameComplement($complementType, $target) {
        if(!isset($this->propertiesNameComplement[$complementType])) { return ''; }
        if(!isset($this->propertiesNameComplement[$complementType][$target])) { return ''; }
        return $this->propertiesNameComplement[$complementType][$target];
    }

    public function getPropertiesWithNameComplement(): array
    {
        $properties = array_merge(
            $this->getPropertiesNameComplement()['prefix'],
            $this->getPropertiesNameComplement()['suffix']
        );
        foreach ($properties as $property => $value) { $properties[$property] = $property; }

        return $properties;
    }

    /**
     * @return array
     */
    public function getPropertiesNameComplement(): array
    {
        return $this->propertiesNameComplement;
    }

    /**
     * @param array $propertiesNameComplement
     * @return BaseObject
     */
    public function setPropertiesNameComplement(array $propertiesNameComplement): BaseObject
    {
        $this->propertiesNameComplement = $propertiesNameComplement;
        return $this;
    }

    /**
     * @return string
     */
    public function getMultipleName(): string
    {
        if(!$this->multipleName) { return $this::getClassShortName().'s'; }
        return $this->multipleName;
    }

    /**
     * @param string $multipleName
     * @return BaseObject
     */
    public function setMultipleName(string $multipleName): BaseObject
    {
        $this->multipleName = $multipleName;
        return $this;
    }

}
