<?php

namespace Project\Utilities;

class DateConverter {

    static function toFr($date, $format = 'd/m/Y à H:i'): string
    {
        if(is_string($date)) { $date = strtotime($date); }

        return date($format, $date);
    }

    static function toDatetime($date)
    {
        return static::toFr($date, 'Y-m-d H:i:s');
    }

    static function diff($dateStart = null, $dateEnd = null)
    {
        if(!$dateStart) { $dateStart = static::newDatetime(); }
        if(!$dateEnd) { $dateEnd = static::newDatetime(); }

        $dateStart =  new \DateTime($dateStart);
        $dateEnd =  new \DateTime($dateEnd);

        $diff = $dateStart->diff($dateEnd);

        return $diff;
    }

    static function newDatetime()
    {
        return date('Y-m-d H:i:s');
    }

}


