<?php


namespace Project\Utilities\Middleware;


use Project\PageManager;
use Project\Utilities\DirectoryManipulator;

class WeberTools
{
    private static $sourcePath = '';
    private static $outputPath = '';

    private static $collection = [];

    static public function getClassPath($class)
    {
        return static::getFullSourcePath(sprintf("%s/%s.js", $class['namespace'], $class['name']));
    }

    static public function getFullSourcePath($path): string
    {
        return sprintf('%s/%s', static::getSourcePath(), $path);
    }

    static private function generateOutput()
    {
        $output = [];
        foreach (static::$collection as $key => $class) { $output[] = $class; }

        if(!file_exists(static::getOutputPath())) {
            file_put_contents(static::getOutputPath(), json_encode($output));
            chmod(static::getOutputPath(), 0774);
            return true;
        }
        file_put_contents(static::getOutputPath(), json_encode($output));
    }

    static private function loadAnnotations()
    {
        foreach (static::$collection as $key => $class) {
            $path = static::getClassPath($class);
            if(!file_exists($path)) { continue; }
            $content = file_get_contents($path);
            preg_match_all('/@?extends (?<dependencies>[A-z_]+)/', $content, $matches);
            foreach ($matches['dependencies'] as $dependency) {
                if(!isset(static::$collection[$dependency])) { continue; }

                $targetDependency = static::$collection[$dependency];
                $class['dependencies'][
                sprintf('%s/%s',
                    $targetDependency['namespace'], $targetDependency['name'])
                ] = 1;
            }

            preg_match('/@instantiate (?<value>true|false|1|0)/', $content, $match);
            if($match) { $class['instantiate'] = boolval($match['value']); }


            static::$collection[$key] = $class;
        }
    }

    static public function loadDirectoryContent($directory = '')
    {
        if (!is_dir($directory)) { return false; }
        $directoryContent = DirectoryManipulator::getDirectory($directory);

        foreach ($directoryContent as $directoryElement) {
            $path = $directory.'/'.$directoryElement;

            if(is_dir($path)) { static::loadDirectoryContent($path);  continue;  }
            if(!str_ends_with($directoryElement, '.js')) { continue; }

            $namespace = str_replace(static::getSourcePath(), '', $directory);
            if (str_starts_with($namespace, '/')) { $namespace = substr($namespace, 1); }

            $fileName = str_replace('.js', '', $directoryElement);

            static::$collection[$fileName] = [
                'namespace' => $namespace,  'name' => $fileName,
                'dependencies' => [],  'instantiate' => false
            ];
        }

        return true;
    }

    static public function generate($sourcePath = '', $outputPath = '')
    {
        static::$collection = [];
        static::configuration($sourcePath, $outputPath);
        static::loadDirectoryContent(static::getSourcePath());

        static::loadAnnotations();

        static::generateOutput();
    }

    static public function configuration($sourcePath = '', $outputPath = '')
    {
        static::setSourcePath(PageManager::getDirectoryAbsoluteRoot() . $sourcePath);
        static::setOutputPath(PageManager::getDirectoryAbsoluteRoot() . $outputPath);
    }

    /**
     * @return string
     */
    public static function getSourcePath(): string
    {
        return static::$sourcePath;
    }

    /**
     * @param string $sourcePath
     */
    public static function setSourcePath(string $sourcePath): void
    {
        static::$sourcePath = $sourcePath;
    }

    /**
     * @return string
     */
    public static function getOutputPath(): string
    {
        return static::$outputPath;
    }

    /**
     * @param string $outputPath
     */
    public static function setOutputPath(string $outputPath): void
    {
        static::$outputPath = $outputPath;
    }


    public static function addToCollection($element)
    {
        static::$collection[] = $element;
    }

    /**
     * @return array
     */
    public static function getCollection(): array
    {
        return static::$collection;
    }

    /**
     * @param array $collection
     */
    public static function setCollection(array $collection): void
    {
        static::$collection = $collection;
    }
}