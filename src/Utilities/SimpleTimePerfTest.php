<?php

namespace Project\Utilities;

class SimpleTimePerfTest
{
     protected $started_time = 0;
     protected $ended_time = 0;
     protected $testInProcess = false;

     public function __construct()
     {
         $this->startTest();
     }

    public function reset()
    {
        $this->setStartedTime(0);
        $this->setEndedTime(0);
        $this->setTestInProcess(false);
    }

    public function startTest()
    {
        if($this->isTestInProcess()) { $this->reset(); }

        $this->setTestInProcess(true);
        $this->setStartedTime(microtime(true));
    }


    public function endTest()
    {
        $this->setEndedTime(microtime(true));
        $this->setTestInProcess(false);
    }

    public function getTestResult($scale = 'ms', $rounded = 2)
    {
        if($this->isTestInProcess()) { $this->endTest(); }

        //Scale 1 is in seconds
        $ratio = 1;
        switch ($scale) {
            case 's' : $ratio = 1; break;
            case 'ms' : $ratio = 1000; break;
            case 'us' : $ratio = 10000; break;
            default : $ratio = $scale; break;
        }

        if(!is_float($ratio) && !is_int($ratio)) { $ratio = 1; }

        $result = ($this->getEndedTime() - $this->getStartedTime()) * $ratio;
        if($rounded) { $result = round($result, $rounded); }

        return $result;
    }

    /*-----------*/

    /**
     * @return bool
     */
    public function isTestInProcess()
    {
        return $this->testInProcess;
    }

    /**
     * @param bool $testInProcess
     */
    public function setTestInProcess(bool $testInProcess)
    {
        $this->testInProcess = $testInProcess;
    }

    /**
     * @return float
     */
    public function getStartedTime(): float
    {
        return $this->started_time;
    }

    /**
     * @param float $started_time
     */
    public function setStartedTime(float $started_time): void
    {
        $this->started_time = $started_time;
    }

    /**
     * @return float
     */
    public function getEndedTime(): float
    {
        return $this->ended_time;
    }

    /**
     * @param float $ended_time
     */
    public function setEndedTime(float $ended_time): void
    {
        $this->ended_time = $ended_time;
    }

}
