<?php
namespace Project\Utilities;

use Project\PageManager;

abstract class BaseSmartyWrapper extends AbstractRenderer
{
    static protected $Smarty;

    static protected $compile_dir;
    static protected $config_dir;
    static protected $cache_dir;
    static protected $plugin_dir;

    static protected $cache_lifetime = 10;
    static protected $display_cache_id;

    static protected $config_vars = [];

    abstract static public function initialize($testmode = false);

    abstract static protected function loaderHookEvent();

    public static function loader()
    {
        static::loadThemeConfigurations();
        static::loadThemeStyles();

        static::loaderHookEvent();

        static::getSmarty()->config_vars = static::getConfigVars();
        static::setConfigVars([]);
    }


    public static function getCompiledStylePath($filePath = false): string
    {
        if (!$filePath) { return ''; }

        if(!is_dir(static::getStyleCompilationDir())) { mkdir(static::getStyleCompilationDir()); }

        $compiledPath = sprintf('%s%s.css', static::getStyleCompilationDir(), $filePath);
        $publicCompiledPath = PageManager::getAsset('styles', sprintf('compiled/%s.css', $filePath));
        if (file_exists($compiledPath)) {
            $lastModification = filemtime($compiledPath);
            $delta = time() - $lastModification;
            if($delta <=  -1) { return $publicCompiledPath; }
            if($delta <= static::getCacheLifetime()) { return $publicCompiledPath;}

            unlink($compiledPath);
        }

        $output = false;
        $rawDefaultFilePath = sprintf('%s/assets/styles/%s.css', static::getTemplateDefaultDir(), $filePath);

        $configs = static::getSmarty()->getConfigVars();
        static::getSmarty()->config_vars = static::getConfigVars();

        if(file_exists($rawDefaultFilePath)) {
            $css = static::getSmarty()->fetch($rawDefaultFilePath);
            file_put_contents($compiledPath, $css, FILE_APPEND);
            $output = true;
        }

        if(static::getThemeName() !== 'default') {
            $rawFilePath = sprintf('%s/assets/styles/%s.css', static::getTemplateDir(), $filePath);
            if (file_exists($rawFilePath)) {
                $css = static::getSmarty()->fetch($rawFilePath);
                file_put_contents($compiledPath, $css, FILE_APPEND);
                $output = true;
            }
        }

        static::getSmarty()->config_vars = $configs;

        if(!$output) { return ''; }

        return $publicCompiledPath;
    }

    static public function loadThemeStyles()
    {
        $styleToLoad = [];
        static::getSmarty()->configLoad(static::getThemeName().'.conf', 'styles');

        foreach (static::getSmarty()->getConfigVars() as $ConfigKey => $ConfigFile) {
            $path = static::getCompiledStylePath($ConfigFile);
            static::getSmarty()->clearConfig($ConfigKey);

            $styleToLoad[] = $path;
        }

        static::setStylesList($styleToLoad);
    }

    static public function loadDefaultThemeConfigurations(bool $includeDefault = true)
    {
        if(!$includeDefault) { return false; }
        if(!file_exists(sprintf('%s/default.conf', static::getConfigDir()))) { return []; }

        $configSectionIncluded = [];
        static::getSmarty()->configLoad('default.conf', 'configs');

        foreach (static::getSmarty()->getConfigVars() as $ConfigKey => $ConfigFile) {
            $confPath = sprintf('default/%s.conf', $ConfigFile);
            if(!file_exists(sprintf('%s/%s', static::getConfigDir(), $confPath))) { continue; }

            static::getSmarty()->configLoad(sprintf('default/%s.conf', $ConfigFile));
            static::getSmarty()->clearConfig($ConfigKey);

            $configSectionIncluded[$ConfigKey] = $ConfigFile;
        }

        return $configSectionIncluded;
    }

    static public function loadThemeConfigurations(bool $override = true, bool $includeDefault = true)
    {
        $theme_name = static::getThemeName();

        $defaultConfigsIncluded = static::loadDefaultThemeConfigurations($includeDefault);
        static::updateConfigVars(static::getSmarty()->config_vars);

        if($theme_name === 'default') { return true; }
        if(!file_exists(sprintf('%s/%s.conf', static::getConfigDir(), $theme_name))) { return false; }

        static::getSmarty()->configLoad(sprintf('%s.conf', $theme_name), 'configs');
        foreach (static::getSmarty()->getConfigVars() as $ConfigKey => $ConfigFile) {
            if(isset($defaultConfigsIncluded[$ConfigKey]) && !$override) {
                static::getSmarty()->clearConfig($ConfigKey);
                continue;
            }

            $confPath = sprintf('%s/%s.conf', $theme_name, $ConfigFile);
            if(!file_exists(sprintf('%s/%s', static::getConfigDir(), $confPath))) {
                static::getSmarty()->clearConfig($ConfigKey);
                continue;
            }

            static::getSmarty()->configLoad($confPath);
            static::getSmarty()->clearConfig($ConfigKey);
        }

        static::updateConfigVars(static::getSmarty()->config_vars);
        return true;
    }

    static public function isCurrentRenderCached($params = [], $forceReload = false)
    {
        if(!static::getSmarty()->caching) { return false; }
        $Route = PageManager::getRouter()->getCurrentRoute();
        if(!$Route || !$Route->getFile()) { return false; }

        if(!is_array($params)) { $params = [$params]; }
        $cacheGroup = implode('', $params);

        if($forceReload) { static::clearCache($Route->getFile(), $cacheGroup); return false; }
        if(!$cacheGroup) { $cacheGroup = null; }

        return static::getSmarty()->isCached(sprintf('%s.tpl', $Route->getFile()), $cacheGroup);
    }

    static public function setInsertWrapperBinder($wrapperCallback = false) {
        if(!$wrapperCallback) { return false; }
        if(!is_callable($wrapperCallback)) { return false; }

        return $wrapperCallback();
    }


    static public function insertManager($args = [])
    {
        if(!isset($args['method'])) { return; }
        $method = $args['method'];
        unset($args['method']);
        if(!method_exists(static::class, $method)) { return; }
        return static::$method($args);
    }


    static public function isCached($filename)
    {
        return static::getSmarty()->isCached($filename);
    }

    static public function clearCache($filename = null, $cacheGroup = null)
    {
        if(!$filename && !$cacheGroup) { static::getSmarty()->clearAllCache(); return;  }
        static::getSmarty()->clearCache($filename, $cacheGroup);
    }


    static public function render($file)
    {
        static::getSmarty()->display( $file, static::getDisplayCacheId());
        static::setDisplayCacheId(null);
    }

    static public function assign($key, $value)
    {
        static::getSmarty()->assign($key, $value);
    }

    static public function getAssigned($key)
    {
        return static::getSmarty()->getTemplateVars($key);
    }

    /**
     * Get the value of Smarty
     * @return \Smarty
     */
    static public function getSmarty()
    {
        return static::$Smarty;
    }

    /**
     * Set the value of Smarty
     *
     * @returnstatic
     */
    static public function setSmarty($Smarty)
    {
        static::$Smarty = $Smarty;
    }

    static public function setDisplayCacheId($cache_id = null)
    {
        static::$display_cache_id = $cache_id;
    }

    static public function getDisplayCacheId()
    {
        return static::$display_cache_id;
    }

    /**
     * Get the value of compile_dir
     */
    static public function getCompileDir()
    {
        return static::$compile_dir;
    }

    /**
     * Set the value of compile_dir
     *
     */
    static public function setCompileDir($compile_dir)
    {
        static::$compile_dir = $compile_dir;
    }

    /**
     * Get the value of config_dir
     */
    static public function getConfigDir()
    {
        return static::$config_dir;
    }

    /**
     * Set the value of config_dir
     *
     */
    static public function setConfigDir($config_dir)
    {
        static::$config_dir = $config_dir;
    }

    /**
     * Get the value of cache_dir
     */
    static public function getCacheDir()
    {
        return static::$cache_dir;
    }

    /**
     * Set the value of cache_dir
     *
     */
    static public function setCacheDir($cache_dir)
    {
        static::$cache_dir = $cache_dir;
    }

    /**
     * @return int
     */
    public static function getCacheLifetime(): int
    {
        return static::$cache_lifetime;
    }

    /**
     * @param int $cache_lifetime
     */
    public static function setCacheLifetime(int $cache_lifetime): void
    {
        static::$cache_lifetime = $cache_lifetime;
    }

    /**
     * @return mixed
     */
    public static function getPluginDir()
    {
        return static::$plugin_dir;
    }

    /**
     * @param mixed $plugin_dir
     */
    public static function setPluginDir($plugin_dir): void
    {
        static::$plugin_dir = $plugin_dir;
    }

    public static function updateConfigVars($config_vars = [], bool $flushSmarty = true)
    {
        static::setConfigVars(array_merge(static::getConfigVars(), $config_vars));
        if($flushSmarty) { static::getSmarty()->config_vars = []; }
    }


    /**
     * @return array
     */
    public static function getConfigVars(): array
    {
        return static::$config_vars;
    }

    /**
     * @param array $config_vars
     */
    public static function setConfigVars(array $config_vars): void
    {
        static::$config_vars = $config_vars;
    }
    
}