<?php
namespace Project\Utilities;

class BaseRenderer extends AbstractRenderer
{
    static protected $assignVariables = [];

    static public function initialize()
    {
    }

    static public function render($file)
    {
        foreach (static::$assignVariables as $key => $var) {
            if(!$key) { continue; }
            ${$key} = $var;
        }

        include( $file );
        return;
    }

    static public function assign($varName, $value)
    {
        static::$assignVariables[$varName] = $value;
    }

    static public function getAssigned($key)
    {
        if(!isset(static::$assignVariables[$key])) { return null; }
        return static::$assignVariables[$key];
    }
}