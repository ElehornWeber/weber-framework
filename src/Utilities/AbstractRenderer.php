<?php
namespace Project\Utilities;

abstract class AbstractRenderer
{
    static protected $renderPath = '';
    static protected $template_default_dir;
    static protected $template_dir;
    static protected $theme_name = 'default';
    static protected $style_compilation_dir = '';
    static protected $base_compilation_dir = '';
    static protected $styles_list = [];

    abstract static public function initialize();
    abstract static public function render($file);
    abstract static public function getAssigned($key);

    static public function renderer($file)
    {
        $filePath = $file;
        if(static::getRenderPath()) { $filePath = sprintf('%s/%s',static::getRenderPath(), $file); }
        if(!file_exists($filePath) && static::getThemeName() !== 'default' ) {
            $defaultFilePath = sprintf('%s/%s', static::getTemplateDefaultDir(), $file);
            if(!file_exists($defaultFilePath)) { return false; }
            $filePath = $defaultFilePath;
        }
        return static::render($filePath);
    }

    abstract static public function assign($varName, $value);
    static public function assignArray($array)
    {
        if(!is_array($array)) { return false; }
        foreach($array as $key => $value) {
            if(!$key) { continue; }
            static::assign($key, $value);
        }
    }


    /**
     * @return string
     */
    public static function getThemeName(): string
    {
        return static::$theme_name;
    }

    /**
     * @param string $theme_name
     */
    public static function setThemeName(string $theme_name): void
    {
        static::$theme_name = $theme_name;
    }

    /**
     * @return string
     */
    public static function getRenderPath()
    {
        return static::$renderPath;
    }

    /**
     * @param string $renderPath
     */
    public static function setRenderPath(string $renderPath)
    {
        static::$renderPath = $renderPath;
    }

    /**
     * @return mixed
     */
    public static function getTemplateDefaultDir()
    {
        return static::$template_default_dir;
    }

    /**
     * @param mixed $template_default_dir
     */
    public static function setTemplateDefaultDir($template_default_dir): void
    {
        static::$template_default_dir = $template_default_dir;
    }

    /**
     * @return array
     */
    public static function getStylesList(): array
    {
        return static::$styles_list;
    }

    /**
     * @param array $styles_list
     */
    public static function setStylesList(array $styles_list): void
    {
        static::$styles_list = $styles_list;
    }

    /**
     * Get the value of template_dir
     */
    static public function getTemplateDir()
    {
        return static::$template_dir;
    }

    /**
     * Set the value of template_dir
     *
     */
    static public function setTemplateDir($template_dir)
    {
        static::$template_dir = $template_dir;
    }


    /**
     * @return string
     */
    public static function getStyleCompilationDir(): string
    {
        return static::$style_compilation_dir;
    }

    /**
     * @param string $style_compilation_dir
     */
    public static function setStyleCompilationDir(string $style_compilation_dir): void
    {
        static::$style_compilation_dir = $style_compilation_dir;
    }

    /**
     * @return string
     */
    public static function getBaseCompilationDir(): string
    {
        return static::$base_compilation_dir;
    }

    /**
     * @param string $base_compilation_dir
     */
    public static function setBaseCompilationDir(string $base_compilation_dir): void
    {
        static::$base_compilation_dir = $base_compilation_dir;
    }


}