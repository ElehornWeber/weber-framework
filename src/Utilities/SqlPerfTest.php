<?php


namespace Project\Utilities;

class SqlPerfTest
{
   static protected $queries = [];

   public static function getQueriesAmount()
   {
       return count(static::$queries);
   }

   /*
    * Every connections should be in the same format to make this work
    */
   public static function getTotalExecutionTime()
   {
       $total = 0;
       foreach (static::getQueries() as $query) { $total += $query['exec']; }
       return $total;
   }

   /*********************/

    /**
     * @return array
     */
    public static function getQueries(): array
    {
        return static::$queries;
    }

    public static function addQuery($source, $table, $methodName, $exec, $additionalInfos = false)
    {
        if(!$additionalInfos) {
            $additionalInfos = [
                'request' => '',
                'condition' => '',
                'additional' => ''
            ];
        }

        static::$queries[] = [
            'source' => $source,
            'table' => $table,
            'methodName' => $methodName,
            'exec' => $exec,
            'additionalInfos' => $additionalInfos
        ];
    }

    /**
     * @param array $queries
     */
    public static function setQueries(array $queries): void
    {
        static::$queries = $queries;
    }
}
