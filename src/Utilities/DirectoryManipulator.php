<?php


namespace Project\Utilities;

class DirectoryManipulator
{
    /*
     *  We slice the 2 first entries of the dir. Because on linux the 2 first entries are '.' and '..'
     */
    static public function getDirectory($directoryPath, $sliceNeeded = true): array
    {
        if(!is_dir($directoryPath)) { return []; }

        if(!$sliceNeeded) { return scandir($directoryPath); }

        return array_slice(scandir($directoryPath), 2);
    }
}