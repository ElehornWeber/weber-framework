<?php

namespace Project\Dao;

/*
 * Dynamic cache is a simple static method that store AbstractDb and manuals results in it.
 * Thanks to it, we can avoid duplicate request inside the current context.
 * It's not a long-live cache method but it's still useful.
 * @Todo Make an option to make the Dynamic Cache Persistant by Session => one UserSide or find a way to make it GLOBAL (multi-user caching!)
 */

use Project\Dao\Builder\CoreBuilder;
use Project\Models\AbstractDbClass;
use Project\Security\ConnexionManager;
use Project\Utilities\SimpleDataPerfTest;
use Project\Utilities\SimpleTimePerfTest;

class DynamicCache {
    static private $Cache = [];
    static private $AdvancedCache = [];
    static private $ConditionMapper = [];

    static public $Count = 0;
    static public $PersistentCount = 0;

    static public $persistent = 30;
    static public $cacheLoadingTime = 0;

    public static $updateCacheTime = 0;
    static public $addToCacheTime = 0;

    public static function getStats(): array
    {
        return [
            'cacheSize' => count(static::$Cache,  COUNT_RECURSIVE),
            'advancedCacheSize' => count(static::$AdvancedCache,  COUNT_RECURSIVE),
            'conditionMappingSize' => count(static::$ConditionMapper,  COUNT_RECURSIVE),

            'Count' => static::$Count,
            'PersistentCount' => static::$PersistentCount,

            'updateCacheTime' => static::$updateCacheTime,
            'addToCacheTime' => static::$addToCacheTime,
            'cacheLoadingTime' => static::$cacheLoadingTime,
            'totalCacheUsingTime' => static::$updateCacheTime+static::$addToCacheTime+static::$cacheLoadingTime
        ];
    }

    static public function initializePersistentCache()
    {
        if(!static::$persistent) {
            static::destroyPersistentCache();
            return false;
        }

        $cacheTime = ConnexionManager::getFromSession('dynamic_cache_time');

        if(!$cacheTime) {
            ConnexionManager::setToSession('dynamic_cache_time', 0);
            $cacheTime = 0;
        }
        if(time() - $cacheTime > static::$persistent) {
            static::destroyPersistentCache();
            ConnexionManager::setToSession('dynamic_cache_time', time());
        }

        $Cache = ConnexionManager::getFromSession('dynamic_cache_store');

        static::$Cache = $Cache['Simple'];;
        static::$AdvancedCache = $Cache['Advanced'];;
        static::$ConditionMapper = $Cache['ConditionMapper'];;

        return true;
    }

    static protected function getFromPersistent(string $type, $ModelName)
    {
        if($type === 'Advanced' && isset(static::$AdvancedCache[$ModelName])) {
            return static::$AdvancedCache[$ModelName];
        }
        if($type === 'Simple' && isset(static::$Cache[$ModelName])) {
            return static::$Cache[$ModelName];
        }

        $Cache = ConnexionManager::getFromSession('dynamic_cache_store');
        if(isset($Cache[$type]) && isset($Cache[$type][$ModelName])) {
            return $Cache[$type][$ModelName];
        }

        return false;
    }

    static public function getModelFromCacheById($ModelName, $id)
    {
        $perf = new SimpleTimePerfTest();

        $ModelCache = static::getFromPersistent('Simple', $ModelName);

        if(!isset($ModelCache[$ModelName])) { return null; }
        if(!isset($ModelCache[$ModelName][$id])) { return null; }

        SimpleDataPerfTest::incrementData('dynamic_cache_object', 1);
        static::$Count++;
        static::$cacheLoadingTime += $perf->getTestResult();
        return $ModelCache[$ModelName][$id];
    }

    static public function getResultsFromAdvancedCache($ModelName, $Condition)
    {
        $perf = new SimpleTimePerfTest();

        $buildedCondition = '';
        if(is_array($Condition)) { $buildedCondition = AbstractDao::buildCondition($Condition); }
        else if($Condition instanceof CoreBuilder) { $buildedCondition = $Condition->formatSelect(); }

        $ModelCache = static::getFromPersistent('Advanced', $ModelName);
        if(isset($ModelCache[$buildedCondition])) {
            SimpleDataPerfTest::incrementData('dynamic_cache_object', 1);
            static::$PersistentCount++;
            static::$cacheLoadingTime += $perf->getTestResult();
            return $ModelCache[$buildedCondition];
        }

        if($Condition instanceof CoreBuilder && $Condition->isSingle() && $Condition->getCondition()) {
            /**@var AbstractDbClass $Model */
            $Model = new $ModelName();
            if($Condition->getCondition()->getProperty() === $Model->getUniqueIdProperty()) {
                return static::getModelFromCacheById($ModelName, $Condition->getCondition()->getValue());
            }
        }

        return null;
    }

    static public function destroyPersistentCache()
    {
        ConnexionManager::setToSession('dynamic_cache_store', [
            'Advanced' => [],
            'Simple' => [],
            'ConditionMapper' => [],
        ]);
    }

    static public function destroyCache()
    {
        static::destroyPersistentCache();
        static::$Cache = [];
        static::$ConditionMapper = [];
    }


    static protected function removeFromAdvancedCache(AbstractDbClass $Model, $TargetModels, string $key)
    {
        /**@todo improve advanced cache: store only ID and get the real object into SimpleCache**/
        unset(static::$AdvancedCache[$Model::getClassCompleteName()]);
        return true;

        if(!is_array($TargetModels)) { $TargetModels = [ $TargetModels->getUniquePropertyValue() => $TargetModels ]; }
        if(isset($TargetModels[$Model->getUniquePropertyValue()])) {
            unset($TargetModels[$Model->getUniquePropertyValue()]);
            static::$AdvancedCache[$Model::getClassCompleteName()][$key] = $TargetModels;
            return true;
        }

        if(count($TargetModels) === 0) { unset(static::$AdvancedCache[$Model::getClassCompleteName()][$key]); return true; }
        if(count($TargetModels) === 1) { static::$AdvancedCache[$Model::getClassCompleteName()][$key] = $TargetModels[0]; return true; }



        return false;
    }

    static public function removeFromCache(AbstractDbClass $Model)
    {
        if(isset(static::$Cache[$Model::getClassCompleteName()])) {
            if(isset(static::$Cache[$Model::getClassCompleteName()][$Model->getUniquePropertyValue()]))
            unset(static::$Cache[$Model::getClassCompleteName()][$Model->getUniquePropertyValue()]);
            static::updateCache('Simple');
        }

        $advancedModified = false;
        if(isset(static::$AdvancedCache[$Model::getClassCompleteName()])) {
            foreach (static::$AdvancedCache[$Model::getClassCompleteName()] as $key => $targetModel) {
                $result = static::removeFromAdvancedCache($Model, $targetModel, $key);
                if ($result) {
                    $advancedModified = true;
                }
            }
        }
        if($advancedModified) { static::updateCache('Advanced'); }
    }

    static public function updateCache($cacheKey = '')
    {
        if(!static::$persistent) { return false; }

        $perf = new SimpleTimePerfTest();
        $Cache = ConnexionManager::getFromSession('dynamic_cache_store');
        if(!$Cache) { $Cache = []; }

        if(!isset($Cache['Advanced'])) { $Cache['Advanced'] = []; }
        if(!isset($Cache['Simple'])) { $Cache['Simple'] = []; }
        if(!isset($Cache['ConditionMapper'])) { $Cache['ConditionMapper'] = []; }

        if($cacheKey && isset($Cache[$cacheKey])) {
            $associatedCache = null;
            switch ($cacheKey) {
                case 'Simple' : $associatedCache = static::$Cache; break;
                case 'Advanced' : $associatedCache = static::$AdvancedCache; break;
                case 'ConditionMapper' : $associatedCache = static::$ConditionMapper; break;
            }
            $Cache[$cacheKey] = array_merge($Cache[$cacheKey], $associatedCache);
            ConnexionManager::setToSession('dynamic_cache_store', $Cache);

            switch ($cacheKey) {
                case 'Simple' : static::$Cache = $Cache[$cacheKey]; break;
                case 'Advanced' : static::$AdvancedCache = $Cache[$cacheKey]; break;
                case 'ConditionMapper' : static::$ConditionMapper = $Cache[$cacheKey]; break;
            }

            static::$updateCacheTime += $perf->getTestResult();

            return true;
        }

        $Cache['Advanced'] = array_merge($Cache['Advanced'], static::$AdvancedCache);
        $Cache['Simple'] = array_merge($Cache['Simple'], static::$Cache);
        $Cache['ConditionMapper'] = array_merge($Cache['ConditionMapper'], static::$ConditionMapper);

        static::$AdvancedCache = $Cache['Advanced'];
        static::$Cache = $Cache['Simple'];
        static::$ConditionMapper = $Cache['ConditionMapper'];

        ConnexionManager::setToSession('dynamic_cache_store', $Cache);
        static::$updateCacheTime += $perf->getTestResult();

        return true;
    }

    /**
     * @param $ModelName
     * @param AbstractDbClass $Object
     * @return bool
     */
    static public function addToDynamicCache($ModelName, $Object)
    {
        $perf = new SimpleTimePerfTest();
        if(!$Object instanceof AbstractDbClass) { return false; }

        if(!isset(static::$Cache[$ModelName])) { static::$Cache[$ModelName] = []; }
        static::$Cache[$ModelName][$Object->getUniquePropertyValue()] = $Object;

        static::$addToCacheTime += $perf->getTestResult();
        static::updateCache('Simple');
    }

    /**
     * @param $ModelName
     * @param AbstractDbClass $Object
     * @param CoreBuilder $Builder
     * @return bool
     */
    static public function addToDynamicAdvancedCache($ModelName, $Results, CoreBuilder $Builder)
    {
        $buildedCondition = $Builder->formatSelect();

        $perf = new SimpleTimePerfTest();
        if(!isset(static::$AdvancedCache[$ModelName])) { static::$AdvancedCache[$ModelName] = []; }

        foreach ($Results as $Result) {
            static::addToDynamicCache($ModelName, $Result);
        }

        if($Builder->isSingle() && $Results) { $Results = $Results[0]; }

        static::$AdvancedCache[$ModelName][$buildedCondition] = $Results;

        static::$addToCacheTime += $perf->getTestResult();
        static::updateCache('Advanced');
    }

    public static function getCacheByModel($ModelName): array
    {
        if(!isset(static::$Cache[$ModelName])) { return []; }
        return static::$Cache[$ModelName];
    }

    /**
     * @return array
     */
    public static function getCache(): array
    {
        return static::$Cache;
    }

    /**
     * @return array
     */
    public static function getAdvancedCache(): array
    {
        return static::$AdvancedCache;
    }

    /**
     * @return array
     */
    public static function getConditionMapper(): array
    {
        return static::$ConditionMapper;
    }

    public static function getPersistentCache()
    {
        return ConnexionManager::getFromSession('dynamic_cache_store');
    }

    /**
     * @param array $Cache
     */
    public static function setCache(array $Cache): void
    {
        self::$Cache = $Cache;
    }

    /**
     * @param array $AdvancedCache
     */
    public static function setAdvancedCache(array $AdvancedCache): void
    {
        self::$AdvancedCache = $AdvancedCache;
    }
}