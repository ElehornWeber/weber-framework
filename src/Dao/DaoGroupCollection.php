<?php
namespace Project\Dao;


class DaoGroupCollection
{
    private $defaultDatabase = '';
    private $DaoGroupCollection = [];
    private $baseDaoPath = '';

    public function getGroupByDatabase($databaseName = '')
    {
        if(isset($this->DaoGroupCollection[$databaseName])) {
            return $this->DaoGroupCollection[$databaseName];
        }
        if(isset($this->DaoGroupCollection[$this->getDefaultDatabase()])) {
            return $this->DaoGroupCollection[$this->getDefaultDatabase()];
        }
        return new DaoGroup();
    }

    public function addDaoGroupToCollection(DaoGroup $daoGroup)
    {
        $this->DaoGroupCollection[$daoGroup->getDatabaseName()] = $daoGroup;
    }

    /**
     * @return array
     */
    public function getDaoGroupCollection(): array
    {
        return $this->DaoGroupCollection;
    }

    /**
     * @param array $DaoGroupCollection
     * @return DaoGroupCollection
     */
    public function setDaoGroupCollection(array $DaoGroupCollection): DaoGroupCollection
    {
        $this->DaoGroupCollection = $DaoGroupCollection;
        return $this;
    }

    /**
     * @return string
     */
    public function getDefaultDatabase(): string
    {
        return $this->defaultDatabase;
    }

    /**
     * @param string $defaultDatabase
     * @return DaoGroupCollection
     */
    public function setDefaultDatabase(string $defaultDatabase): DaoGroupCollection
    {
        $this->defaultDatabase = $defaultDatabase;
        return $this;
    }

    /**
     * @return string
     */
    public function getBaseDaoPath(): string
    {
        if(!$this->baseDaoPath)  { return __DIR__; }
        return $this->baseDaoPath;
    }

    /**
     * @param string $baseDaoPath
     * @return DaoGroupCollection
     */
    public function setBaseDaoPath(string $baseDaoPath): DaoGroupCollection
    {
        $this->baseDaoPath = $baseDaoPath;
        return $this;
    }
}