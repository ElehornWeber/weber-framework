<?php


namespace Project\Dao\Builder\Group;


class Single extends AbstractGroup
{
    public function __construct(string $property = '')
    {
        $this->setProperty($property);
    }

    public function generate(): string
    {
        return sprintf('`%s`', $this->getProperty());
    }
}