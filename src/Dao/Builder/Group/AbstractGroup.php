<?php


namespace Project\Dao\Builder\Group;


use Project\Dao\Builder\AbstractBuilderElement;

abstract class AbstractGroup extends AbstractBuilderElement
{
    private $property = '';

    /**
     * @return string
     */
    public function getProperty(): string
    {
        return $this->property;
    }

    /**
     * @param string $property
     * @return AbstractGroup
     */
    public function setProperty(string $property): AbstractGroup
    {
        $this->property = $property;
        return $this;
    }


}