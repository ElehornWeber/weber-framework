<?php


namespace Project\Dao\Builder;


use Project\Dao\Builder\Condition\AbstractCondition;
use Project\Dao\Builder\Group\AbstractGroup;
use Project\Dao\Builder\Limit\AbstractLimit;
use Project\Dao\Builder\Order\AbstractOrder;
use Project\Dao\Builder\Selection\AbstractSelection;
use Project\Dao\CoreLibrary;

class CoreBuilder
{
    private $prefix = '';
    private $methodName = '';
    private $tableName = '';
    private $isSingle = null;

    private $Selection = null;
    private $Condition = null;
    private $Order = null;
    private $Limit = null;
    private $Group = null;


    public function formatSelect()
    {
        return CoreLibrary::generateSelect(
            $this->getTableName(),
            $this->getSelection(),
            $this->getCondition(),
            $this->getLimit(),
            $this->getOrder(),
            $this->getGroup()
        );
    }

    public function addToBuilder($Parameter)
    {
        if($Parameter instanceof AbstractSelection) { return $this->setSelection($Parameter); }
        if($Parameter instanceof AbstractCondition) { return $this->setCondition($Parameter); }
        if($Parameter instanceof AbstractOrder) { return $this->setOrder($Parameter); }
        if($Parameter instanceof AbstractLimit) { return $this->setLimit($Parameter); }
        if($Parameter instanceof AbstractGroup) { return $this->setGroup($Parameter); }

        return $this;
    }

    public function mergeSimpleBuilder(array $builder, $allowOverride = true)
    {
        if($allowOverride || (!$allowOverride && !$this->getPrefix())) { $this->setPrefix($builder['prefix']); }
        if($allowOverride || (!$allowOverride && !$this->getMethodName())) { $this->setMethodName($builder['methodName']); }

        if($allowOverride || (!$allowOverride && $this->isSingle === null)) {
            $this->setIsSingle($builder['isSingle']);
            if($this->isSingle()) { $this->setLimit(new Limit\Single(1)); }
        }
    }

    public function isValid(): bool
    {


        return true;
    }

    /**
     * @return string
     */
    public function getPrefix(): string
    {
        return $this->prefix;
    }

    /**
     * @param string $prefix
     * @return CoreBuilder
     */
    public function setPrefix(string $prefix): CoreBuilder
    {
        $this->prefix = $prefix;
        return $this;
    }

    /**
     * @return string
     */
    public function getMethodName(): string
    {
        return $this->methodName;
    }

    /**
     * @param string $methodName
     * @return CoreBuilder
     */
    public function setMethodName(string $methodName): CoreBuilder
    {
        $this->methodName = $methodName;
        return $this;
    }

    /**
     * @return bool
     */
    public function isSingle(): bool
    {
        if($this->isSingle === null) { return true; }
        return $this->isSingle;
    }

    /**
     * @param bool $isSingle
     * @return CoreBuilder
     */
    public function setIsSingle(bool $isSingle): CoreBuilder
    {
        $this->isSingle = $isSingle;
        return $this;
    }

    /**
     * @return Condition\AbstractCondition
     */
    public function getCondition()
    {
        return $this->Condition;
    }

    /**
     * @param AbstractCondition $Condition
     * @return CoreBuilder
     */
    protected function setCondition(AbstractCondition $Condition)
    {
        if($this->Condition instanceof  AbstractCondition) {
            $this->Condition->addCondition($Condition);
            return $this;
        }
        $this->Condition = $Condition;
        return $this;
    }

    /**
     * @return Order\AbstractOrder
     */
    public function getOrder()
    {
        return $this->Order;
    }

    /**
     * @param AbstractOrder $Order
     * @return CoreBuilder
     */
    protected function setOrder(AbstractOrder $Order)
    {
        $this->Order = $Order;
        return $this;
    }

    /**
     * @return Limit\AbstractLimit
     */
    public function getLimit()
    {
        return $this->Limit;
    }

    /**
     * @param AbstractLimit $Limit
     * @return CoreBuilder
     */
    protected function setLimit(AbstractLimit $Limit)
    {
        $this->Limit = $Limit;
        return $this;
    }

    /**
     * @return Group\AbstractGroup
     */
    public function getGroup()
    {
        return $this->Group;
    }

    /**
     * @param null $Group
     * @return CoreBuilder
     */
    protected function setGroup($Group)
    {
        $this->Group = $Group;
        return $this;
    }

    /**
     * @return Selection\AbstractSelection
     */
    public function getSelection()
    {
        return $this->Selection;
    }

    /**
     * @param null $Selection
     * @return CoreBuilder
     */
    public function setSelection($Selection)
    {
        $this->Selection = $Selection;
        return $this;
    }

    /**
     * @return string
     */
    public function getTableName(): string
    {
        return $this->tableName;
    }

    /**
     * @param string $tableName
     * @return CoreBuilder
     */
    public function setTableName(string $tableName): CoreBuilder
    {
        $this->tableName = $tableName;
        return $this;
    }


}