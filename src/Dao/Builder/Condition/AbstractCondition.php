<?php
namespace Project\Dao\Builder\Condition;


use Project\Dao\Builder\AbstractBuilderElement;

abstract class AbstractCondition extends AbstractBuilderElement
{
    private $alternativeConditions = [];
    private $mustConditions = [];
    private $equalConditions = [];


    protected abstract function generateSingle(): string;

    public function generateAll(): string
    {
        if(!$this->hasSubConditions()) { return $this->generateSingle(); }

        $finalCondition = $this->generateSingle();
        $finalCondition = $this->mergeConditions($finalCondition, 'alternative');
        $finalCondition = $this->mergeConditions($finalCondition);

        $finalCondition = $this->mergeEqualsConditions($finalCondition);

        return $finalCondition;
    }

    public function generate(): string
    {
        return $this->generateAll();
    }

    protected function mergeEqualsConditions(string $finalCondition)
    {
        foreach ($this->equalConditions as $equalCondition) {
            /**@var AbstractCondition $equalCondition*/

            $finalCondition = sprintf('%s OR %s', $finalCondition, $equalCondition->generate());
        }

        return $finalCondition;
    }

    protected function mergeConditions(string $finalCondition, string $type = 'must')
    {
        $conditions = $this->getMustConditions();
        if($type === 'alternative') { $conditions = $this->getAlternativeConditions(); }

        if(!$conditions) { return $finalCondition; }

        /**@var AbstractCondition $ConditionLine*/
        $conditionsBuilder = [];
        foreach ($conditions as $ConditionLine) {
            $conditionsBuilder[] = $ConditionLine->generateAll();
        }

        $conditionsBuilder[] = $finalCondition;

        $mergeWording = 'AND';
        if($type === 'alternative') { $mergeWording = 'OR'; }

        return sprintf('(%s)', implode(sprintf(" %s ", $mergeWording), $conditionsBuilder));
    }

    public function addCondition(AbstractCondition $Condition, $type = 'must'): AbstractCondition
    {
        if($type === 'alternative' || $type === 'OR') { return $this->addAlternativeCondition($Condition); }
        if($type === 'equal' || $type === 'EQ') { return $this->addEqualCondition($Condition); }
        return $this->addMustCondition($Condition);
    }

    protected function hasSubConditions(): bool
    {
        if(!empty($this->mustConditions)) { return true; }
        if(!empty($this->alternativeConditions)) { return true; }

        return false;
    }

    protected function addAlternativeCondition(AbstractCondition $Condition): AbstractCondition
    {
        $this->alternativeConditions[] = $Condition;
        return $this;
    }
    protected function addEqualCondition(AbstractCondition $Condition): AbstractCondition
    {
        $this->equalConditions[] = $Condition;
        return $this;
    }

    protected function addMustCondition(AbstractCondition $Condition): AbstractCondition
    {
        $this->mustConditions[] = $Condition;
        return $this;
    }

    /**
     * @return array
     */
    public function getAlternativeConditions(): array
    {
        return $this->alternativeConditions;
    }

    /**
     * @param array $alternativeConditions
     * @return AbstractCondition
     */
    public function setAlternativeConditions(array $alternativeConditions): AbstractCondition
    {
        $this->alternativeConditions = $alternativeConditions;
        return $this;
    }

    /**
     * @return array
     */
    public function getMustConditions(): array
    {
        return $this->mustConditions;
    }

    /**
     * @param array $mustConditions
     * @return AbstractCondition
     */
    public function setMustConditions(array $mustConditions): AbstractCondition
    {
        $this->mustConditions = $mustConditions;
        return $this;
    }
}