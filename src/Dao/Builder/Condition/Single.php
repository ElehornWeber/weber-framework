<?php
namespace Project\Dao\Builder\Condition;
use Project\Dao\Builder\Map\OperandSymbolMap;
class Single extends AbstractCondition
{
    private $property = '';
    private $operand = 'EQUAL';
    private $value;
    private $type = 'string';
    public function __construct(string $property, $value, $operand = 'EQUAL')
    {
        $this->setProperty($property)
            ->setValue($value)
            ->setOperand(strtoupper($operand))
            ->setType(gettype($value));
    }
    public function isValid(): bool
    {
        if($this->getProperty() === '') { return false; }
        if(!$this->getOperand()) { return false; }
        if($this->getType() !== gettype($this->getValue())) { return false; }
        return true;
    }
    protected function generateIn(): string
    {
        $values = $this->getValue();
        if(!is_array($values)) { $values = [$values]; }
        $formatQuery = sprintf('"%s"', implode('","', $values));
        return sprintf('`%s` IN (%s)', $this->getProperty(), $formatQuery);
    }
    protected function generateLike(): string
    {
        return sprintf('`%s` LIKE "%s"', $this->getProperty(), $this->formatValue());
    }
    protected function generateIS(): string
    {
        return sprintf('`%s` IS %s', $this->getProperty(), $this->formatValue());
    }
    protected function formatValue()
    {
        switch($this->getType()) {
            case 'integer': case 'boolean':
            return intval($this->getValue()); break;
            case 'float':
                return floatval($this->getValue()); break;
            case 'NULL':
                return 'NULL'; break;
            default: case 'string':
            return sprintf('"%s"', $this->getValue()); break;
        }
    }
    protected function generateDefault(): string
    {
        $operandSymbol = OperandSymbolMap::getValue($this->getOperand());
        $baseCondition = sprintf('`%s`%s', $this->getProperty(), $operandSymbol);
        return $baseCondition.$this->formatValue();
    }
    public function generateSingle(): string
    {
        switch ($this->getOperand()) {
            case 'LIKE': return $this->generateLike();
            case 'IN': return $this->generateIn();
            case 'IS': return $this->generateIS();
            default : return $this->generateDefault();
        }
    }
    /**
     * @return string
     */
    public function getProperty(): string
    {
        return $this->property;
    }
    /**
     * @param string $property
     * @return Single
     */
    public function setProperty(string $property): Single
    {
        $this->property = $property;
        return $this;
    }
    /**
     * @return string
     */
    public function getOperand(): string
    {
        return $this->operand;
    }
    /**
     * @param string $operand
     * @return Single
     */
    public function setOperand(string $operand): Single
    {
        $this->operand = $operand;
        return $this;
    }
    /**
     * @return mixed
     */
    public function getValue()
    {
        return $this->value;
    }
    /**
     * @param mixed $value
     * @return Single
     */
    public function setValue($value)
    {
        $this->value = $value;
        return $this;
    }
    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }
    /**
     * @param string $type
     * @return Single
     */
    public function setType(string $type): Single
    {
        $this->type = $type;
        return $this;
    }
}