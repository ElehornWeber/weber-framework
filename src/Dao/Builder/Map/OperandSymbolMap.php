<?php
namespace Project\Dao\Builder\Map;

use Core\AbstractMap;

class OperandSymbolMap extends AbstractMap
{
    static protected $map = [
        'EQUAL' => '=',
        'NOT.EQUAL' => '!=',
        'INFERIOR' => '<',
        'INFERIOR.EQUAL' => '<=',
        'SUPERIOR' => '>',
        'SUPERIOR.EQUAL' => '>='
    ];

    static protected $alias = [
        'EQ' => 'EQUAL',
        'INF' => 'INFERIOR', 'INF.EQ' => 'INFERIOR.EQUAL',
        'SUP' => 'SUPERIOR', 'SUP.EQ' => 'SUPERIOR.EQUAL',
        'NOT' => 'NOT.EQUAL'
    ];

    public static function getValue(string $key)
    {
        $key = strtoupper($key);
        $key = str_replace(' ', '.', $key);
        return parent::getValue($key);
    }
}