<?php


namespace Project\Dao\Builder;


abstract class AbstractBuilderElement
{
    abstract public function generate(): string;
}