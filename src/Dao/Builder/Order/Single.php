<?php
namespace Project\Dao\Builder\Order;


class Single extends AbstractOrder
{

    public function __construct($property, $type = 'ASC', $nextOrder = null)
    {
        $this->setProperty($property);
        $this->setType($type);
        $this->setNextOrder($nextOrder);
    }

    public function generate(): string
    {
        $nestingGeneration = '';
        if($this->getNextOrder()) {
            $nestingGeneration = sprintf(',%s', $this->getNextOrder()->generate());
        }
        return sprintf('`%s` %s%s', $this->getProperty(), $this->getType(), $nestingGeneration);
    }
}