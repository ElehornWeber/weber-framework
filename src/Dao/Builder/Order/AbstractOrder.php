<?php


namespace Project\Dao\Builder\Order;


use Project\Dao\Builder\AbstractBuilderElement;

abstract class AbstractOrder extends AbstractBuilderElement
{
    private $property = '';
    private $type = 'ASC';
    private $nextOrder = null;

    /**
     * @return string
     */
    public function getProperty(): string
    {
        return $this->property;
    }

    /**
     * @param string $property
     * @return AbstractOrder
     */
    public function setProperty(string $property): AbstractOrder
    {
        $this->property = $property;
        return $this;
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * @param string $type
     * @return AbstractOrder
     */
    public function setType(string $type): AbstractOrder
    {
        $this->type = strtoupper($type);
        return $this;
    }

    /**
     * @return null|AbstractOrder
     */
    public function getNextOrder()
    {
        return $this->nextOrder;
    }

    /**
     * @param AbstractOrder $nextOrder
     * @return AbstractOrder
     */
    public function setNextOrder($nextOrder)
    {
        if(!$nextOrder instanceof AbstractOrder) { return $this; }

        $this->nextOrder = $nextOrder;
        return $this;
    }
}