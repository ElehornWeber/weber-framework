<?php


namespace Project\Dao\Builder\Limit;


class Single extends AbstractLimit
{
    public function __construct(int $amount = 0)
    {
        $this->setAmount($amount);
    }

    public function generate(): string
    {
        return $this->getAmount();
    }
}