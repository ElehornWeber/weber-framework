<?php


namespace Project\Dao\Builder\Limit;


use Project\Dao\Builder\AbstractBuilderElement;

abstract class AbstractLimit extends AbstractBuilderElement
{
    private $amount = 0;

    /**
     * @return int
     */
    public function getAmount(): int
    {
        return $this->amount;
    }

    /**
     * @param int $amount
     * @return AbstractLimit
     */
    public function setAmount(int $amount): AbstractLimit
    {
        $this->amount = $amount;
        return $this;
    }
}