<?php


namespace Project\Dao\Builder\Selection;


use Project\Dao\Builder\AbstractBuilderElement;

abstract class AbstractSelection extends AbstractBuilderElement
{
    private $property = '';
    private $alias = '';
    private $formula = '';
    private $type = '';

    private $additionalSelections = [];

    abstract public function __construct($property, $type = '', $alias = '', $formula = '');

    public function addAdditionalSelection($property, $type = '', $alias = '', $formula = '')
    {
        $this->additionalSelections[] = new Single($property, $type, $alias, $formula);
    }

    /**
     * @return array
     */
    public function getAdditionalSelections(): array
    {
        return $this->additionalSelections;
    }

    /**
     * @return string
     */
    public function getProperty(): string
    {
        return $this->property;
    }

    /**
     * @param string $property
     * @return AbstractSelection
     */
    public function setProperty(string $property): AbstractSelection
    {
        $this->property = $property;
        return $this;
    }

    /**
     * @return string
     */
    public function getAlias(): string
    {
        return $this->alias;
    }

    /**
     * @param string $alias
     * @return AbstractSelection
     */
    public function setAlias(string $alias): AbstractSelection
    {
        $this->alias = $alias;
        return $this;
    }

    /**
     * @return string
     */
    public function getFormula(): string
    {
        return $this->formula;
    }

    /**
     * @param string $formula
     * @return AbstractSelection
     */
    public function setFormula(string $formula): AbstractSelection
    {
        $this->formula = $formula;
        return $this;
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * @param string $type
     * @return AbstractSelection
     */
    public function setType(string $type): AbstractSelection
    {
        $this->type = $type;
        return $this;
    }


}