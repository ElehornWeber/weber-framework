<?php


namespace Project\Dao\Builder\Selection;


class Single extends AbstractSelection
{
    public function __construct($property, $type = '', $alias = '', $formula = '')
    {
        $this->setProperty($property);
        $this->setType($type);
        $this->setFormula($formula);
        $this->setAlias($alias);
    }


    public function generate(): string
    {
        $selection = sprintf('`%s`', $this->getProperty());
        if($this->getFormula()) { $selection = $this->getFormula(); }
        if($this->getType()) { $selection = sprintf('%s(%s)', $this->getType(), $selection); }
        if($this->getAlias()) { $selection = sprintf('%s as %s', $selection, $this->getAlias()); }

        foreach ($this->getAdditionalSelections() as $SingleSelection)
        {
            /**@var Single $SingleSelection*/
            $selection = sprintf('%s, %s', $selection, $SingleSelection->generate());
        }

        return $selection;
    }
}