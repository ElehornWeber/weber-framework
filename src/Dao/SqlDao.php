<?php
namespace Project\Dao;

class SqlDao extends DaoGroup
{
    public function __construct($databaseHost = '', $user = '', $password = '', $databaseName = '')
    {
        $databaseHost = getenv($databaseHost);
        $user = getenv($user);
        $password = getenv($password);
        $databaseName = getenv($databaseName);

        if(!$databaseHost || !$user || !$databaseName) { return false; }

        $this->initializeConnexion($databaseHost, $user, $password, $databaseName);

        if ($this->getMysqli()->connect_errno) {
            echo printf("Échec de la connexion : %s\n", $this->getMysqli()->connect_error);
            exit();
        }

        $this->setDaoPath(__DIR__);
    }

}