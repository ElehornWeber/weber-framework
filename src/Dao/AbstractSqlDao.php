<?php
namespace Project\Dao;

use Project\Models\AbstractDbClass;
use Project\Utilities\DataConverter;
use Project\Utilities\DirectoryManipulator;
use Project\Utilities\ErrorCatcher;
use ReflectionClass;

abstract class DaoGroup extends AbstractDao
{
    private $databaseName = '';
    private $Daos = [];
    private $DaoPath = '';

    private function getDaosCollectionFromModels()
    {
        $Models = [];
        $ModelCollectionPath = $this->getDaoPath().'/../Models/';
        foreach (DirectoryManipulator::getDirectory($ModelCollectionPath) as $ModelFile) {
            $ModelPath = $ModelCollectionPath.$ModelFile;
            if(!is_dir($ModelPath)) { $Models[] = str_replace('.php', '', $ModelFile); continue; }
            foreach (DirectoryManipulator::getDirectory($ModelPath) as $ModelSubPathFile) {
                $Models[] =  str_replace('.php', '', sprintf('%s', $ModelSubPathFile));
            }
        }

        return $Models;
    }

    private function autoloadDaoFromModel($DaoName, $nameSpace = '')
    {
        if($this->checkIfDaoExist($DaoName)) { return; }

        $fullModel = sprintf('Project\Models\%s',$DaoName);

        if($nameSpace) { $fullModel = sprintf('Project\Models\%s\%s', $nameSpace, $DaoName); }

        /**@var $model AbstractDbClass*/
        $model = new $fullModel();

        if(!class_exists($fullModel)) { return false; }

        $tableName = $model->getTableName();
        if(!$model->getTableName()) {
            $tableName = DataConverter::convertToSnakeCase($DaoName);
        }

        $newDao = new AbstractDao();
        $newDao->mysqli = $this->mysqli;
        $newDao->tableName = $tableName;
        if($model->getMultipleName()) { $newDao->multipleName = $model->getMultipleName(); }
        $newDao->modelName = $fullModel;
        $newDao->modelShortName = $DaoName;

        if($this->addDao($DaoName, $newDao)) { return $newDao; }
        return false;
    }

    private function addDao($name, AbstractDao $Dao)
    {
        $request = sprintf(
            'SHOW TABLES IN %s LIKE "%s"',
            $this->getDatabaseName(),
            DataConverter::convertToSnakeCase($Dao->tableName)
        );
        $query = $this->mysqli->query($request);
        if(!$query->fetch_assoc()) {
            ErrorCatcher::addClassErrorMessage(
                'DaoGroup',
                'addDao',
                [sprintf('Dao %s tried to bind himself to %s but the table does not exist', $Dao->modelName, $Dao->tableName)],
                new \Error()
            );
            return false;
        }
        $this->Daos[$name] = $Dao;

        return $Dao;
    }

    public function getDao($name, $namespace = ''): AbstractDao
    {
        if(!isset($this->Daos[$name])) {
            $autoloadedDao = $this->autoloadDaoFromModel($name, $namespace);
            if(!$autoloadedDao) { return $this; }
            return $autoloadedDao;
        }
        return $this->Daos[$name];
    }

    public function checkIfDaoExist($name): bool
    {
        if(!isset($this->Daos[$name])) { return false; }
        return true;
    }

    public function findDaoFromClass($class)
    {
        if(!class_exists($class)) { return $this; }
        $className = (new ReflectionClass($class))->getShortName();

        if($className === $class) { return $this->getDao($className); }

        $class = sprintf('%s&end', $class);
        $nameSpacePart = str_replace(sprintf('\%s&end' ,$className), '', $class);
        $nameSpacePart = str_replace('Project\\Models\\', '', $nameSpacePart);
        $nameSpacePart = str_replace('Project\\Models', '', $nameSpacePart);

        return $this->getDao($className, $nameSpacePart);
    }

    /**
     * @return array
     */
    public function getDaos(): array
    {
        return $this->Daos;
    }

    /**
     * @param array $Daos
     * @return DaoGroup
     */
    public function setDaos(array $Daos): DaoGroup
    {
        $this->Daos = $Daos;
        return $this;
    }

    /**
     * @return string
     */
    public function getDaoPath(): string
    {
        return $this->DaoPath;
    }

    /**
     * @param string $DaoPath
     * @return DaoGroup
     */
    public function setDaoPath(string $DaoPath): DaoGroup
    {
        $this->DaoPath = $DaoPath;
        return $this;
    }

    /**
     * @return string
     */
    public function getDatabaseName(): string
    {
        return $this->databaseName;
    }

    /**
     * @param string $databaseName
     * @return DaoGroup
     */
    public function setDatabaseName(string $databaseName): DaoGroup
    {
        $this->databaseName = $databaseName;
        return $this;
    }
}