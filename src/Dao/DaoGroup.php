<?php
namespace Project\Dao;

use mysqli_sql_exception;
use mysqli;
use Project\Models\AbstractDbClass;
use Project\PageManager;
use Project\Utilities\DataConverter;
use Project\Utilities\DirectoryManipulator;
use Project\Utilities\ErrorCatcher;
use ReflectionClass;

class DaoGroup
{
    private $mysqli;
    private $databaseName = '';
    private $Daos = [];
    private $DaoPath = '';
    private $BaseDaoPath = '';

    /**
     * @throws mysqli_sql_exception
     */
    public function initializeConnexion(string $host, string $user, $password = '', $databaseName = '')
    {
        if($databaseName) { $this->setDatabaseName($databaseName); }

        mysqli_report(MYSQLI_REPORT_STRICT);
        try {
            $connexion = new mysqli($host, $user, $password, $this->getDatabaseName());
            $this->setMysqli($connexion);
        } catch (mysqli_sql_exception $e ) {
            ErrorCatcher::addError($e);
            throw $e;
        }

    }

    public function getDaoFromModelFile($ModelFile, $nameSpace = '')
    {
        $ModelFile = str_replace('.php', '', $ModelFile);

        if($nameSpace) { $nameSpace .= '/'; }

        $nameSpace = str_replace('/', '\\', $nameSpace);
        $ModelNameSpace = sprintf(
            '%s\\%s\%s\%s',
            AbstractDbClass::getBaseNameSpace(),
            $this->getDaoPath(),
            $nameSpace,
            $ModelFile
        );
        $ModelNameSpace = str_replace('\\\\', '\\', $ModelNameSpace);

        return $this->autoloadDaoFromModel($ModelNameSpace);
    }

    public function getDaosCollectionFromModels($path = '')
    {
        $Models = [];
        $basePath = sprintf('%s/src/Models/%s',PageManager::getDirectoryAbsoluteRoot(), $this->getDaoPath());
        $fullPath = $basePath;
        if($path) { $fullPath = sprintf('%s/%s', $basePath, $path); }

        foreach (DirectoryManipulator::getDirectory($fullPath) as $ModelFile) {
            $ModelPath = $fullPath.'/'.$ModelFile;

            $nameSpace = $ModelFile;
            if($path) { $nameSpace = sprintf('%s/%s', $path, $ModelFile); }

            if(is_dir($ModelPath)) {
                $Models = array_merge($Models, $this->getDaosCollectionFromModels($nameSpace));
                continue;
            }

            $Dao = $this->getDaoFromModelFile($ModelFile, $path);

            if(!$Dao) { continue; }
            $Models[$Dao->getModelShortName()] = $Dao;
        }

        return $Models;
    }

    private function getModelByDaoName($DaoName = '', $nameSpace = '') {
        if(isset($this->Daos[$DaoName])) {
            /**@var AbstractDao $Dao*/
            $Dao = $this->Daos[$DaoName];
            return $Dao->getModelName();
        }

        $fullModel = sprintf('Project\Models');

        if($this->getDaoPath()) {
            $fullModel = sprintf('%s\%s', $fullModel, $this->getDaoPath());
        }
        if($nameSpace) {
            $fullModel = sprintf('%s\%s', $fullModel, $nameSpace);
        }
        $fullModel = sprintf('%s\%s', $fullModel, $DaoName);

        if(!class_exists($fullModel)) { return false; }
        return new $fullModel();
    }

    private function autoloadDaoFromModel($Model)
    {
        if(!is_object($Model) && is_string($Model) && class_exists($Model)) { $Model = new $Model(); }

        if(!$Model instanceof AbstractDbClass) { return false; }

        if(!$this->getMysqli()) { return false; }
        if($this->checkIfDaoExist($Model::getClassShortName())) { return false; }

        $tableName = $Model->getTableName();
        if(!$Model->getTableName()) {
            $tableName = DataConverter::convertToSnakeCase($Model::getClassShortName());
        }

        $ModelDao = AbstractDao::class;

        $Dao = new $ModelDao();
        $modelDao = sprintf('%s\\%s\\%s', $this::getNamespace(), $this->getDaoPath(), $Model::getClassShortName());
        if($this->getDaoPath()) {
            if (class_exists($modelDao)) { $Dao = new $modelDao();}
            else if (class_exists(sprintf('%sDao', $modelDao))) {
                $modelDao = sprintf('%sDao', $modelDao);
                $Dao = new $modelDao();
            }
        }

        if(!$Dao instanceof AbstractDao) { return false; }

        $Dao->setMysqli($this->getMysqli());
        $Dao->setTableName($tableName);
        if($Model->getMultipleName()) { $Dao->setMultipleName($Model->getMultipleName()); }
        $Dao->setModelName($Model::getClassCompleteName());
        $Dao->setModelShortName($Model::getClassShortName());

        if($this->addDao($Dao->getModelShortName(), $Dao)) { return $Dao; }
        return false;
    }

    private function addDao($name, AbstractDao $Dao)
    {
        $request = sprintf(
            'SHOW TABLES IN %s LIKE "%s"',
            $this->getDatabaseName(),
            DataConverter::convertToSnakeCase($Dao->getTableName())
        );

        /**@var mysqli $query */
        $query = $this->getMysqli()->query($request);
        if(!$query->fetch_assoc()) {
            ErrorCatcher::addClassErrorMessage(
                'DaoGroup',
                'addDao',
                [sprintf('Dao %s tried to bind himself to %s but the table does not exist', $Dao->getModelName(), $Dao->getTableName())],
                new \Error()
            );
            return false;
        }

        $Dao->setMysqli($this->getMysqli());

        $this->Daos[$name] = $Dao;

        return $Dao;
    }

    /**
     * @param string $name
     * @param null|AbstractDbClass $Model
     * @param string $namespace
     */
    public function getDao($Model = null, $name = '', $namespace = '')
    {
        if(!$Model) {
            $Model = $this->getModelByDaoName($name, $namespace);
            if(!$Model) { return false; }
        }

        if(!isset($this->Daos[$Model::getClassShortName()])) {
            $autoloadedDao = $this->autoloadDaoFromModel($Model);
            if(!$autoloadedDao) { return false; }
            return $autoloadedDao;
        }

        return $this->Daos[$Model::getClassShortName()];
    }

    public function checkIfDaoExist($name): bool
    {
        if(!isset($this->Daos[$name])) { return false; }
        return true;
    }

    public function findDaoFromClass($class)
    {
        if($class instanceof AbstractDbClass) {
            return $this->getDao($class);
        }
        if(class_exists($class)) {
            return $this->getDao(new $class());
        }

        return false;
    }

    public function getFullDaoPath()
    {
        if(!$this->getBaseDaoPath()) { return $this->getDaoPath(); }

        return $this->getBaseDaoPath().'/'.$this->getDaoPath();
    }

    /**
     * @return array
     */
    public function getDaos(): array
    {
        return $this->Daos;
    }

    /**
     * @param array $Daos
     * @return DaoGroup
     */
    public function setDaos(array $Daos): DaoGroup
    {
        $this->Daos = $Daos;
        return $this;
    }

    /**
     * @return string
     */
    public function getDaoPath(): string
    {
        return $this->DaoPath;
    }

    /**
     * @param string $DaoPath
     * @return DaoGroup
     */
    public function setDaoPath(string $DaoPath): DaoGroup
    {
        $this->DaoPath = $DaoPath;
        return $this;
    }

    /**
     * @return string
     */
    public function getDatabaseName(): string
    {
        return $this->databaseName;
    }

    /**
     * @param string $databaseName
     * @return DaoGroup
     */
    public function setDatabaseName(string $databaseName): DaoGroup
    {
        $this->databaseName = $databaseName;
        return $this;
    }

    /**
     * @return mysqli
     */
    public function getMysqli()
    {
        return $this->mysqli;
    }

    /**
     * @param mixed $mysqli
     * @return DaoGroup
     */
    public function setMysqli($mysqli)
    {
        $this->mysqli = $mysqli;
        return $this;
    }

    /**
     * @return string
     */
    public function getBaseDaoPath(): string
    {
        return $this->BaseDaoPath ?? __DIR__;
    }

    /**
     * @param string $BaseDaoPath
     * @return DaoGroup
     */
    public function setBaseDaoPath(string $BaseDaoPath): DaoGroup
    {
        $this->BaseDaoPath = $BaseDaoPath;
        return $this;
    }

    public static function getNamespace()
    {
        $reflect = new ReflectionClass(static::class);
        return $reflect->getNamespaceName();
    }

}