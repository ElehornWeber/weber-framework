<?php


namespace Project\Dao;


use Project\Dao\Builder\Condition\AbstractCondition;
use Project\Dao\Builder\Group\AbstractGroup;
use Project\Dao\Builder\Limit\AbstractLimit;
use Project\Dao\Builder\Order\AbstractOrder;
use Project\Dao\Builder\Selection\AbstractSelection;

class CoreLibrary
{
    public function __construct()
    {

    }

    protected static function generateGroupPart($Group): string
    {
        if(!$Group instanceof AbstractGroup) { return ''; }
        return sprintf('GROUP BY %s', $Group->generate());
    }

    protected static function generateOrderPart($Order): string
    {
        if(!$Order instanceof AbstractOrder) { return ''; }
        /**@var AbstractOrder $Order**/
        return sprintf('ORDER BY %s', $Order->generate());
    }

    protected static function generateLimitPart($Limit): string
    {
        if(!$Limit instanceof AbstractLimit) { return ''; }
        /**@var AbstractLimit $Limit**/
        return sprintf('LIMIT %s', $Limit->generate());
    }


    protected static function generateConditionPart($Condition): string
    {
        if(!$Condition instanceof AbstractCondition) { return ''; }
        /**@var AbstractCondition $Condition**/
        return sprintf('WHERE %s', $Condition->generateAll());
    }

    protected static function generateSelectionPart($Selection): string
    {
        if(!$Selection instanceof AbstractSelection) { return '*'; }
        /**@var AbstractCondition $Condition**/
        return $Selection->generate();
    }


    public static function generateSelect($table, $Selection, $Condition, $Limit, $Order, $Group): string
    {
        return sprintf(
            'SELECT %s FROM `%s` %s %s %s %s',
            static::generateSelectionPart($Selection),
            $table,
            static::generateConditionPart($Condition),
            static::generateOrderPart($Order),
            static::generateGroupPart($Group),
            static::generateLimitPart($Limit)
        );

    }
}