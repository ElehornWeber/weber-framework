<?php
namespace Project\Dao;

use Project\Dao\Builder\AbstractBuilderElement;
use Project\Dao\Builder\Condition\Single;
use Project\Dao\Builder\CoreBuilder;
use Project\Models\AbstractDbClass;
use Project\Security\ConnexionManager;
use Project\Utilities\ErrorCatcher;
use Project\Utilities\SimpleDataPerfTest;
use Project\Utilities\SimpleTimePerfTest;
use Project\Utilities\SqlPerfTest;
use ReflectionClass;

class AbstractDao
{
    /**
     * @var \mysqli $mysqli
     */
    protected $mysqli;
    protected $tableName;
    protected $modelName;
    protected $modelShortName;
    protected $multipleName;

    private $testMode = false;
    private $executionJournal = [];
    protected $currentBuilder = 0;

    protected function addExecutionJournal($builder)
    {
        $this->executionJournal[] = $builder;
    }

    protected function addExecutionJournalToBuilder( $message, $methodName = '', $arguments = '', $journalType = 'error')
    {
        $this->executionJournal[$this->currentBuilder]['journals'][] = [
            'method' => $methodName,
            'arguments' => $arguments,
            'type' => $journalType,
            'message' => $message
        ];
    }

    /**
     * @return mixed
     */
    public function getMultipleName()
    {
        if(!$this->multipleName) {
            return $this->modelShortName.'s';
        }
        return $this->multipleName;
    }

    public function getClassShortName()
    {
        $reflect = new ReflectionClass($this);
        return $reflect->getShortName();
    }

    public function getDir()
    {
        return __DIR__;
    }

    public function isInError($message = ''): bool
    {
        if(!$this->getMysqli() || !$this->getMysqli()->error_list) { return false; }

        $env = getenv('ENV_MODE');
        if( $env == 'production' || $env == 'prod' || $env == 'master' || !$env ) { return true; }

        ConnexionManager::addFlashSessionMessage('danger', $message);
        foreach ($this->getMysqli()->error_list as $error) {
            ConnexionManager::addFlashSessionMessage('danger', sprintf('DB_ERROR_%s : %s', $error['errno'], $error['error']));
        }

        return true;
    }

    public static function buildCondition($conditions)
    {
        if(!$conditions) { return ' 1'; }

        if(!is_array($conditions)) { return sprintf(' %s ', $conditions); }

        foreach ($conditions as $i => $conditionElement) {
            if($conditionElement === null || $conditionElement === false) {
                unset($conditions[$i]); continue;
            }
            if(!is_string($conditionElement)) {
                unset($conditions[$i]);
                continue;
            }
        }
        if(!$conditions) { return ' 1 '; }

        return sprintf(' %s ', implode(' AND ', $conditions));
    }

    protected function getModelFullNamespace()
    {
        return $this->modelName;
    }

    protected function getModelMultipleName(): string
    {
        if($this->multipleName) { return $this->multipleName; }
        return sprintf('%ss', $this->modelShortName);
    }

    protected function abstractDelete(AbstractDbClass $model)
    {
        $Performance = new SimpleTimePerfTest();
        $builder = $this->getCurrentBuilder();

        $modelFullName = $this->getModelFullNamespace();
        $methodFullName = $builder['prefix'].$this->modelName;

        if(!$model->getUniquePropertyValue()) { return false; }

        $condition = sprintf('%s = %d',
            $model->getFullPropertyName($model->getUniqueIdProperty()),
            $model->getUniquePropertyValue()
        );
        $request = sprintf("DELETE FROM %s WHERE %s",  $this->tableName,  $condition);
        $result = $this->mysqli->query($request);

        if($this->isInError('#DB_ERROR_SELECT: Erreur de la supression de '.$this->tableName) || !$result) {
            ErrorCatcher::addClassErrorMessage($this::getClassShortName(),$builder['prefix'].$modelFullName,['error' => $this->mysqli->error], new \Error());

            return false;
        }

        if($result) {
            DynamicCache::removeFromCache($model);
            $setter = $model->formatPropertyToMethod($model->getUniqueIdProperty(), 'set');
            $model->$setter(null);
        }

        SqlPerfTest::addQuery(
            $this::getClassShortName(),
            $this->tableName,
            $methodFullName,
            $Performance->getTestResult(), [
                'request' => 'remove Model',
                'condition' => $condition,
                'additional' => ''
            ]
        );

        return $model;
    }

    public function isModelExists(AbstractDbClass $Model): bool
    {

        $result = $Model::getDao()->{'get'.$Model::getClassShortName()}(
            new Builder\Selection\Single($Model->getUniqueIdProperty()),
            new Builder\Condition\Single($Model->getUniqueIdProperty(), $Model->getUniquePropertyValue())
        );

        if($result) { return true; }
        return false;
    }

    protected function abstractUpdate(AbstractDbClass $model)
    {
        $builder = $this->getCurrentBuilder();

        $namespace = $this->getModelFullNamespace();
        if(!$model instanceof $namespace) { return false; }

        if(!$this->isModelExists($model) && $builder['prefix'] == 'update') {
            //$model->setUniquePropertyValue(null);
            $builder['prefix'] = 'create';
        }

        $this->updateCurrentBuilder($builder);

        $Performance = new SimpleTimePerfTest();

        $modelFullName = $this->getModelFullNamespace();
        $methodFullName = $builder['prefix'].$this->modelName;
        if(!$builder['isSingle']) { $methodFullName = $builder['prefix'].$this->getModelMultipleName(); }

        $allowedProps = $model->getAllowedProperties($builder['prefix']);

        $properties = [];
        foreach ($allowedProps as $property) {
            $propertyValue = $model->dynamicGetterByProp($property);

            //if($property === $model->getUniqueIdProperty()) { continue; }
            if($propertyValue === '') { $propertyValue = null; }

            if(is_null($propertyValue)) { continue; }
            else if(is_bool($propertyValue)) { $propertyValue = intval($propertyValue); }
            else if(is_string($propertyValue)) { $propertyValue = $this->getMysqli()->real_escape_string($propertyValue); }

            $properties[$model->getFullPropertyName($property)] = $propertyValue;
        }

        if(!$properties) { return false; }

        $formatProperties = [];
        foreach ($properties as $property => $value) {
            switch (gettype($value)) {
                case 'integer' : $row = sprintf('%d', $value); break;
                case 'float' : $row = sprintf('%f', $value); break;
                case 'array' : $row = sprintf('"%s"', mysqli_escape_string($this->getMysqli(), json_encode($value))); break;
                default : { $row = sprintf('"%s"', $value); break; }
            }

            $formatProperties[sprintf('`%s`',$property)] = $row;
        }
        $request = '';
        if($builder['prefix'] === 'create') {
            $request = sprintf("INSERT INTO `%s` (%s) VALUES (%s)",
                $this->getTableName(),
                implode(', ', array_keys($formatProperties)),
                implode(', ', $formatProperties)
            );
        }  else if($builder['prefix'] === 'update') {
            $updateReq = [];
            foreach($formatProperties as $property => $value) {
                $updateReq[] = sprintf('%s = %s', $property, $value);
            }

            switch (gettype($model->getUniquePropertyValue())) {
                case 'integer' : $condition = sprintf('%d', $model->getUniquePropertyValue()); break;
                case 'float' : $condition = sprintf('%f', $model->getUniquePropertyValue()); break;
                case 'array' : $condition = sprintf('"%s"', mysqli_escape_string($this->getMysqli(), json_encode($model->getUniquePropertyValue()))); break;
                default : { $condition = sprintf('"%s"', $model->getUniquePropertyValue()); break; }
            }

            $request = sprintf("UPDATE `%s` SET %s WHERE `%s` = %s",
                $this->getTableName(),
                implode(', ', $updateReq),
                $model->getFullPropertyName($model->getUniqueIdProperty()) ,
                $condition
            );
        }

        if(!$this->getMysqli()) {
            $this->addExecutionJournal('Connexion is closed', 'update', '');
            return false;
        }

        $result = $this->getMysqli()->query($request);

        if($this->isInError('#DB_ERROR_SELECT: Erreur de mise à jour de '.$this->getTableName()) || !$result) {
            ErrorCatcher::addClassErrorMessage($this::getClassShortName(),$builder['prefix'].$modelFullName,['error' => $this->getMysqli()->error], new \Error());

            return false;
        }

        SqlPerfTest::addQuery(
            $this::getClassShortName(),
            $this->tableName,
            $methodFullName,
            $Performance->getTestResult(), [
                'request' => $builder['prefix'].' '.$model::getClassShortName(),
                'additional' => ''
            ]
        );

        if($builder['prefix'] == 'create') {
            $setMethod = $model->formatPropertyToMethod($model->getUniqueIdProperty(), 'set');
            if($setMethod) { $model->$setMethod($this->getMysqli()->insert_id); }

            DynamicCache::addToDynamicCache($model::getClassCompleteName(), $model);
            return $this->mysqli->insert_id;
        }

        DynamicCache::addToDynamicCache($model::getClassCompleteName(), $model);
        return true;
    }

    protected function createNewBuilder()
    {
        $this->currentBuilder++;
        $builder = [
            'prefix' => '',
            'methodName' => '',
            'additionals' => [],
            'searchByProps' => false,
            'isSingle' => true,
            'autoEqual' => true,
            'isValid' => false,
            'isV2' => false
        ];
        $this->executionJournal[$this->currentBuilder] = $builder;
        return $builder;
    }

    protected function getCurrentBuilder()
    {
        if(!isset($this->executionJournal[$this->currentBuilder])) {
            $this->createNewBuilder();
        }
        return $this->executionJournal[$this->currentBuilder];
    }

    protected function updateCurrentBuilder($builder) {
        $this->executionJournal[$this->currentBuilder] = $builder;
        return $this->executionJournal[$this->currentBuilder];
    }

    protected function constructAbstractBuilder($methodName)
    {
        $builder = $this->getCurrentBuilder();
        $builder['methodName'] = $methodName;

        $classShortName = $this->modelName;

        if(class_exists($this->modelName)) {
            /**@var AbstractDbClass $model */
            $model = new $this->modelName();
            $classShortName = $model::getClassShortName();
        }

        $searchRegex = sprintf('/(get|create|update|delete)(%s|%s)*/', $this->getModelMultipleName(), $classShortName);
        /**
         * array $matches :
         * 0 => All group catched
         * 1 => prefix
         * 2 => ModelName called (single or multiple)
         */
        preg_match($searchRegex, $methodName, $matches);
        if(!$matches) { return $builder; }

        $builder['prefix'] = $matches[1];

        if(isset($matches[2])) {
            if($matches[2] == $this->getModelMultipleName()) { $builder['isSingle'] = false; }
        }

        $builder['isValid'] = true;

        return $this->updateCurrentBuilder($builder);
    }

    protected function advancedBuilderProcessGet($builder, array $arguments = [])
    {
        $Performance = new SimpleTimePerfTest();

        if(!$this->getMysqli() && !$this->isTestMode()) {
            $this->addExecutionJournalToBuilder('Connexion is closed', 'get', '');
            return false;
        }

        $builder['isV2'] = true;
        $builder['isValid'] = true;

        $modelFullName = $this->getModelFullNamespace();

        $AdvancedBuilder = new CoreBuilder();
        foreach ($arguments as $argument) {
            if($argument instanceof CoreBuilder) { $AdvancedBuilder = $argument; break; }
        }

        $AdvancedBuilder->mergeSimpleBuilder($builder, false);
        $AdvancedBuilder->setTableName($this->getTableName());

        foreach ($arguments as $argument) { $AdvancedBuilder->addToBuilder($argument); }

        $cacheResult = DynamicCache::getResultsFromAdvancedCache($this->getModelName(), $AdvancedBuilder);
        if($cacheResult !== null) { return $cacheResult; }

        $methodFullName = 'get'.$this->getModelName();
        if(!$AdvancedBuilder->isSingle()) { $methodFullName = 'get'.$this->getModelMultipleName(); }

        $result = [];
        if(!$this->isTestMode()) {
            /**@var AbstractDbClass $model*/
            $model = new $modelFullName();
            $buildedRequest = $AdvancedBuilder->formatSelect();

            foreach ($model->getPropertiesWithNameComplement() as $propertyNameComplement) {
                $buildedRequest = str_replace(
                    sprintf('`%s`', $propertyNameComplement),
                    sprintf('`%s`', $model->getFullPropertyName($propertyNameComplement)),
                    $buildedRequest
                );
            }

            /**@var \mysqli_result $result */
            $result = $this->getMysqli()->query($buildedRequest);
            if ($this->isInError('#DB_ERROR_SELECT: Erreur dans la récupération des ' . $this->getTableName()) || !$result) {
                ErrorCatcher::addClassErrorMessage(
                    $this::getClassShortName(), 'get' . $modelFullName, ['error' => $this->mysqli->error], new \Error()
                );
                return [];
            }
            $result = $result->fetch_all(MYSQLI_ASSOC);
        }

        SqlPerfTest::addQuery(
            $this::getClassShortName(),
            $this->getTableName(),
            $methodFullName,
            $Performance->getTestResult(), [
                'Request' => 'get Model Data',
                'Selection' => $AdvancedBuilder->getSelection(),
                'Condition' => $AdvancedBuilder->getCondition(),
                'Order' => $AdvancedBuilder->getOrder(),
                'Limit' => $AdvancedBuilder->getLimit(),
                'Group' => $AdvancedBuilder->getGroup(),
            ]
        );

        if(!$result) {
            if($AdvancedBuilder->isSingle()) { return false; }
            return [];
        }

        if($AdvancedBuilder->getSelection()) {
            if(!$AdvancedBuilder->isSingle() && !$result) { $result = []; }
            DynamicCache::addToDynamicAdvancedCache($this->modelName,  $result, $AdvancedBuilder);
            if($AdvancedBuilder->isSingle() && is_array($result)) { return $result[0]; }
            if(!$AdvancedBuilder->isSingle() && !is_array($result)) { return [$result]; }
            return $result;
        }

        $Objects = [];
        foreach ($result as $i => $element) {
            /** @var AbstractDbClass $Object */
            $Object = new $modelFullName();
            $Object->bindParametersToObject($element);
            $Objects[] = $Object;
        }

        DynamicCache::addToDynamicAdvancedCache($this->modelName,  $Objects, $AdvancedBuilder);

        if($AdvancedBuilder->isSingle()) { return $Objects[0]; }
        return $Objects;
    }

    protected function isBuilderV2($arguments): bool
    {
        foreach ($arguments as $argument) {
            if(!is_object($argument)) { continue; }
            if($argument instanceof CoreBuilder) { return true; }
            if($argument instanceof AbstractBuilderElement) { return true; }
        }

        return false;
    }

    /**
     *
     * @param $methodName . is the called method that doesn't exist on the current Object.
     * @param $arguments . is every parameters in method(a,b,c...)
     * We First search if this is  a single call or multiple call.
     * If not, we return and save an error @see ErrorCatcher
     * Then, we'll call abstractGet with the gathered parameters.
     *
     * @return array|AbstractDbClass|bool
     */
    public function __call($methodName, $arguments)
    {
        if(!$this->getMysqli() && !$this->isTestMode()) { return false; }

        $this->currentBuilder++;

        $modelFullName = $this->getModelFullNamespace();

        if (!class_exists($modelFullName)) {
            $this->addExecutionJournalToBuilder('Class does not exists', $methodName, $arguments, 'error');
            return [];
        }

        $Model = false;
        if(class_exists($modelFullName)) { $Model = new $modelFullName(); }

        /**@var AbstractDbClass $Model*/
        if (!$Model || !$Model instanceof AbstractDbClass) {
            $this->addExecutionJournalToBuilder(
                sprintf('%s is not instance of AbstractDbClass', $modelFullName),
                $methodName, $arguments, 'error'
            );
            return [];
        }

        $builder = $this->constructAbstractBuilder($methodName);

        /*if (!$builder['isValid'] && !$this->isBuilderV2($arguments)) {
            $this->addExecutionJournalToBuilder('Builder is invalid', $methodName, $arguments, 'error');
            $error = new \Error();
            ErrorCatcher::addClassErrorMessage($this::getClassShortName(), $methodName, $arguments, $error);
            return [];
        }*/

        if($builder['prefix'] === 'get') {
            return $this->advancedBuilderProcessGet($builder, $arguments);
        }

        if(in_array($builder['prefix'], ['create', 'update', 'delete'])) {
            if(!isset($arguments[0])) { return false; }
            $model = $arguments[0];
            if(!$model instanceof AbstractDbClass) {
                $this->addExecutionJournalToBuilder('Model in parameter is not instance of AbstractDbClass',
                    $methodName, $arguments, 'error'
                );
                return false;
            }

            if($builder['prefix'] == 'delete') { return $this->abstractDelete($model); }
            return $this->abstractUpdate($model);
        }

        return false;
    }

    /**
     * @return \mysqli
     */
    public function getMysqli()
    {
        return $this->mysqli;
    }

    /**
     * @param \mysqli $mysqli
     * @return AbstractDao
     */
    public function setMysqli(\mysqli $mysqli): AbstractDao
    {
        $this->mysqli = $mysqli;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getTableName()
    {
        return $this->tableName;
    }

    /**
     * @param mixed $tableName
     * @return AbstractDao
     */
    public function setTableName($tableName)
    {
        $this->tableName = $tableName;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getModelName()
    {
        return $this->modelName;
    }

    /**
     * @param mixed $modelName
     * @return AbstractDao
     */
    public function setModelName($modelName)
    {
        $this->modelName = $modelName;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getModelShortName()
    {
        return $this->modelShortName;
    }

    /**
     * @param mixed $modelShortName
     * @return AbstractDao
     */
    public function setModelShortName($modelShortName)
    {
        $this->modelShortName = $modelShortName;
        return $this;
    }

    /**
     * @param mixed $multipleName
     * @return AbstractDao
     */
    public function setMultipleName($multipleName)
    {
        $this->multipleName = $multipleName;
        return $this;
    }

    /**
     * @return bool
     */
    public function isTestMode(): bool
    {
        return $this->testMode;
    }

    /**
     * @param bool $testMode
     * @return AbstractDao
     */
    public function setTestMode(bool $testMode): AbstractDao
    {
        $this->testMode = $testMode;
        return $this;
    }

    /**
     * @return array
     */
    public function getExecutionJournal(): array
    {
        return array_merge_recursive($this->executionJournal, SqlPerfTest::getQueries());
    }

    /**
     * @param array $executionJournal
     * @return AbstractDao
     */
    public function setExecutionJournal(array $executionJournal): AbstractDao
    {
        $this->executionJournal = $executionJournal;
        return $this;
    }
}