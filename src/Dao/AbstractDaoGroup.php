<?php
namespace Project\Dao;

use Project\Utilities\DirectoryManipulator;
use ReflectionClass;

class AbstractDaoGroup extends AbstractDao
{
    private $Daos = [];

    private function addDao($name, AbstractDao $Dao)
    {
        $this->Daos[$name] = $Dao;
    }

    public function getDao($name): AbstractDao
    {
        if(!isset($this->Daos[$name])) { return $this; }
        return $this->Daos[$name];
    }

    public function findDaoFromClass($class)
    {
        $className = (new ReflectionClass($class))->getShortName();
        return $this->getDao($className);
    }
}