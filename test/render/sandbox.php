<?php

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

require __DIR__ . '/../../vendor/autoload.php';

\Project\Utilities\BaseRenderer::assignArray([ 'a' => 1 ]);
\Project\Utilities\BaseRenderer::renderer('template/template.php');