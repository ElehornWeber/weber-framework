<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

require __DIR__ . '/../../vendor/autoload.php';
require_once 'src/Test.php';

use Project\Dao\AbstractDao;
use Project\Dao\Builder\Condition\Single;
use Test\Test;

$AbstractDao = new AbstractDao();
$AbstractDao->setTestMode(true);
$AbstractDao->setModelName(Test::class);
$AbstractDao->setTableName('test');
$AbstractDao->setModelShortName('Test');

$result = $AbstractDao->getTestsByName('coucou', ['order' => [['name', 'desc']]]);

$condition = new Single('age', 13,'SUPERIOR EQUAL');
$b = new Single('firstname','Jean');
$b->addCondition(new Single('name','Albert'),'OR');
$condition->addCondition($b);

//$AbstractDao->getTests($condition);
dump($condition->generateAll());


//dump($AbstractDao->getExecutionJournal());