<?php

namespace Test;

use Project\Models\AbstractDbClass;

class Test extends AbstractDbClass
{
    private $name;

    public function __construct()
    {
        $this->addAllowedProperty('create','name');
        $this->addAllowedProperty('update','name');
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     * @return Test
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }


    public function isComplete(): bool
    {
        // TODO: Implement isComplete() method.
        return true;
    }
}